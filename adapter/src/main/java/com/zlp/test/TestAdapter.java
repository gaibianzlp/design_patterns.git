package com.zlp.test;

import com.zlp.adapter.ListAdapter;
import com.zlp.service.OrderService;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 12:53
 **/
public class TestAdapter {

    public static void main(String[] args) {


        OrderService orderService = new OrderService();
        List<String> list = new ArrayList<>();
        list.add("zhangsan");
        list.add("lisi");
        // 是配置调用
        ListAdapter listAdapter = new ListAdapter(list);
        orderService.forMap(listAdapter);


    }
}
