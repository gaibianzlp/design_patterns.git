package com.zlp.file.adapter;

import com.zlp.file.service.impl.LogWriteDbServiceImpl;
import com.zlp.file.service.impl.LogWriteFileServiceImpl;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 14:26
 **/
public class LogWriteAdapter extends LogWriteFileServiceImpl {

    private LogWriteDbServiceImpl logWriteDbServiceImpl;

    public LogWriteAdapter(LogWriteDbServiceImpl logWriteDbServiceImpl){
        this.logWriteDbServiceImpl = logWriteDbServiceImpl;
    }

    public void logWriteDbService() {
        // 写入本地文件
        this.LogWriteFile();
        // 写入数据库
//        this.logWriteDbService();
        logWriteDbServiceImpl.logWriteDbService();
    }


}
