package com.zlp.file.entity;

import lombok.Data;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 14:14
 **/
@Data
public class LogBean {

    private String log;

    private String logText;

}
