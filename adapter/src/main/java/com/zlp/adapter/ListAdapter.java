package com.zlp.adapter;

import java.util.HashMap;
import java.util.List;

/**
 * @description: ListAdapter适配器
 * @author: LiPing.Zou
 * @create: 2020-05-09 12:46
 **/
public class ListAdapter extends HashMap {

    /**
     * 当前对象List
     */
    private List list;

    public ListAdapter(List list){
        this.list = list;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public Object get(Object key) {
        return list.get(Integer.valueOf(key.toString()).intValue());
    }
}
