package com.zlp.service;

import java.util.Map;

/**
 * @description: OrderService
 * @author: LiPing.Zou
 * @create: 2020-05-09 12:36
 **/
public class OrderService {

    /** 
     * V1 接口支持Map 参数
     * V2 接口支持List 参数
     * @param maps
     * @date:  2020-05-09 12:36
     * @return: void 
     */
    public void forMap(Map maps){

        for (int i = 0; i < maps.size(); i++) {
            System.out.println(maps.get(i));
            
        }
        for (Object o : maps.entrySet()) {
            System.out.println(o);
            
        }

    }


}
