package com.zlp.strategy.impl;

import com.zlp.strategy.PayStategy;
import org.springframework.stereotype.Service;
/**
 * @description:  微信支付方式生成html
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
@Service
public class WechatPaySategyImpl implements PayStategy {


    @Override
    public String toPayHtml(String sategy) {
        System.out.println("微信生成支付html"+sategy);
        return "微信生成支付html"+sategy;
    }
}
