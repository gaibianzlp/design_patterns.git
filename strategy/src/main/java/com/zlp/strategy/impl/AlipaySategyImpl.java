package com.zlp.strategy.impl;

import com.zlp.strategy.PayStategy;
import org.springframework.stereotype.Service;
/**
 * @description:  支付宝支付方式生成html
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
@Service
public class AlipaySategyImpl implements PayStategy {


    @Override
    public String toPayHtml(String sategy) {
        System.out.println("支付宝生成支付html"+sategy);
        return "支付宝生成支付html"+sategy;
    }
}
