package com.zlp.strategy;

/**
 * @description:  抽取共同的策略
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
public interface PayStategy {

    /**
     * @description : 共同行为策略模板
     * @param : [stategy]
     * @date : 2020-05-09 12:31
     * @return : java.lang.String 
     */
    String toPayHtml(String stategy);
}
