package com.zlp.factory;

import com.zlp.enums.PayTypeEnum;
import com.zlp.strategy.PayStategy;

/**
 * @description: 工厂模式调用
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
public class PayFactory {

    public static PayStategy invokePay(String name){

       /* if (PayTypeEnum.ALIPAY.getName().equals(name)){
            PayStategy payStategy = new AlipaySategyImpl();
            return payStategy;
        }else if (PayTypeEnum.WECHAT.getName().equals(name)){
            PayStategy payStategy = new WechatPaySategyImpl();
            return payStategy;
        }else if (PayTypeEnum.UNION.getName().equals(name)){
            PayStategy payStategy = new UnionPaySategyImpl();
            return payStategy;
        }*/

       /*修改反射机制调用方法*/
        try {
            // 1.获取枚举中className
            String className = PayTypeEnum.getEnum(name);
            // statics.使用java反射技术初始化类
            return (PayStategy) Class.forName(className).newInstance();
        }catch (Exception E){
            return null;
        }
    }

}
