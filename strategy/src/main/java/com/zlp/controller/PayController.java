package com.zlp.controller;

import com.zlp.context.PayStrategyContext;
import com.zlp.enums.PayTypeEnum;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: PayController
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
@RestController
public class PayController {

    /**
     * @description : 调用具体 
     * @param : []
     * @date : 2020-05-08 21:48
     * @return : java.lang.String 
     */
    @GetMapping("toPayHtml")
    public String toPayHtml(){
        // 调用阿里支付方式
        String toPayHtml = PayStrategyContext.toPayHtml(PayTypeEnum.ALIPAY.getName());
        return toPayHtml;
    }
}
