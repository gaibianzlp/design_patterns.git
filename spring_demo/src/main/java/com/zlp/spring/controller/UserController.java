package com.zlp.spring.controller;

import com.zlp.spring.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * 测试类
 * @date: 2022/3/14 17:55
 */
public class UserController {

    public static void main(String[] args) {
        //创建spring容器对象
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-config.xml");
        //从IOC容器中获取UserService对象
        UserService userService = applicationContext.getBean("userService", UserService.class);
        userService.addUser();
    }
}
