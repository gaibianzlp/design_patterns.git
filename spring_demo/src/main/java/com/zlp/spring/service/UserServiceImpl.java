package com.zlp.spring.service;

import com.zlp.spring.mapper.UserMapper;

public class UserServiceImpl implements UserService{

    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public void addUser(){
        System.out.println("UserServiceImpl addUser ...");
        userMapper.addUser();
    }
}
