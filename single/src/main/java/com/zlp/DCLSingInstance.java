package com.zlp;

/**
 * @description: 双检锁/双重校验锁
 * @author: LiPing.Zou
 * @create: 2020-05-09 19:55
 **/
public class DCLSingInstance {

    private volatile static DCLSingInstance singleton;
    private DCLSingInstance (){}

    public static DCLSingInstance getSingleton() {
        if (singleton == null) {
            synchronized (DCLSingInstance.class) {
                if (singleton == null) {
                    singleton = new DCLSingInstance();
                }
            }
        }
        return singleton;
    }
}
