package com.zlp;

/**
 * @description: 饿汉式单列
 * @author: LiPing.Zou
 * @create: 2020-05-09 19:33
 **/
public class HungrySingInstance {

    private  static HungrySingInstance instance = new HungrySingInstance();

    //让构造函数为 private，这样该类就不会被实例化,不能被外部调用
    private HungrySingInstance(){}

    public static HungrySingInstance getInstance(){
        System.out.println("单列模式<饿汉模式>......");
        return instance;
    }

    public void showMessage(){
        System.out.println("Hello World!");
    }

    public static void main(String[] args) {
        //获取唯一可用的对象
        HungrySingInstance object = HungrySingInstance.getInstance();
        //显示消息
        object.showMessage();
    }
}
