package com.zlp;

import java.util.Objects;

/**
 * @description: 懒汉式单列
 * @author: LiPing.Zou
 * @create: 2020-05-09 19:33
 **/
public class LazySingInstance {

    private  static LazySingInstance instance;

    //让构造函数为 private，这样该类就不会被实例化,不能被外部调用
    private LazySingInstance(){}

    public static LazySingInstance getInstance(){

        System.out.println("单列模式<饿汉模式>......");
        if (Objects.isNull(instance)){
             instance = new LazySingInstance();
             return instance;
        }
        return instance;
    }

    public void showMessage(){
        System.out.println("Hello World!");
    }

    public static void main(String[] args) {
        //获取唯一可用的对象
        LazySingInstance object = LazySingInstance.getInstance();
        //显示消息
        object.showMessage();
    }
}
