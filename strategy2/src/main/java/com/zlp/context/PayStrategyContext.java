package com.zlp.context;

import com.zlp.factory.PayFactory;
import com.zlp.strategy.PayAbstract;
import org.springframework.util.StringUtils;

/**
 * @description: 上下文调用具体的策略
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
public class PayStrategyContext {

    public static String toPayHtml(String payCode){

        if (StringUtils.isEmpty(payCode)) return "调用支付方式不能为空！";
        PayAbstract payStrategy = PayFactory.invokePay(payCode);
        if (payStrategy == null) return "没有具体策略方法可以调用！";
        return payStrategy.toPayHtml("上下文调用");
    }
}
