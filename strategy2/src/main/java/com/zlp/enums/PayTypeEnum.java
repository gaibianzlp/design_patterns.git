package com.zlp.enums;

/**
 * @description: 支付枚举策略
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
public enum PayTypeEnum {

    WECHAT("WECHAT", "微信"),
    ALIPAY("ALIPAY", "支付宝");



    private String name;
    private String value;

    PayTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static String getEnum(Object name) {
        for (PayTypeEnum value : PayTypeEnum.values()) {

            if (value.name.equals(name)) {
                return value.getValue();
            }
        }
        return null;
    }


}
