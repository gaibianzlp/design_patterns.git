package com.zlp.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author: zhangxiaodong
 * @date: 2022/10/13 14:00
 * @description:
 */
@Builder
@Data
public class LimitInfoDTO {
    /**
     * 剩余限购数
     */
    private Integer remainLimit;

    /**
     * 总限制数
     */
    private Integer totalLimit;
}
