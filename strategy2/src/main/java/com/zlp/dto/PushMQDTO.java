package com.zlp.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author: gubin
 * @date: 2022/9/16 12:03
 * @description:
 */
@Builder
@Data
public class PushMQDTO {
    String promoId;
    String promoKey;
    String reqNo;
    String payType;
    String memberId;
    Integer skuId;
    Integer quantity;
    String createTime;
    Integer point;
    Integer amount;
}
