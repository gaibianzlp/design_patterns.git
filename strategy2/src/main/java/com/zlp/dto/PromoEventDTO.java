package com.zlp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author: gubin
 * @date: 2022/9/20 12:15
 * @description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PromoEventDTO {
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 活动开始时间
     */
    private Date startTime;
    /**
     * 活动结束时间
     */
    private Date endTime;

}
