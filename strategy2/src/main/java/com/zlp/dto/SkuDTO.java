package com.zlp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SkuDTO {

    private Integer id;

    private Integer spuId;

    private String spuName;

    // 价格，单位分
    private Integer price;

    // 数量
    private Integer quantity;

    private Integer stock;

    private String promoId;

    // 限购
    private Integer limit;
}
