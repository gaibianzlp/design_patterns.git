package com.zlp.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: gubin
 * @date: 2022/9/16 12:03
 * @description:
 */
@Builder
@Data
public class PromoOutDTO implements Serializable {

    private String reqNo;
    private String result;
    Integer status;
    private Integer point;
    private SkuDTO skuDTO;

}
