package com.zlp.dto;

import lombok.Data;


@Data
public class PromoContext implements AutoCloseable {
    private static final ThreadLocal<PromoContext> CTX_OCC = new ThreadLocal<>();

    public PromoContext() {
        CTX_OCC.set(this);
    }

    public static PromoContext get() {
        return CTX_OCC.get();
    }

    @Override
    public void close() {
        CTX_OCC.remove();
    }

    String promoId;

    String promoKey;

    String memberId;

    String result;

    String reqNo;

    Integer skuId;

    /**
     * 支付方式
     * OrderPayType
     * 1 混合支付 MIX
     * 2 现金支付 CASH
     */
    String payType;

    /**
     * V值
     */
    Integer point;

    /**
     * 金额
     */
    Integer amount;

    Integer stock;

    Integer status;

}
