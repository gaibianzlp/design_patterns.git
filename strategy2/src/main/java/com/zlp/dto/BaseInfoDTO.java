package com.zlp.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @author: gubin
 * @date: 2022/9/16 12:03
 * @description:
 */
@Builder
@Data
public class BaseInfoDTO {
    private Integer point;
    private SkuDTO skuDTO;
}
