package com.zlp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author: gubin
 * @date: 2022/9/16 12:03
 * @description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class PromoInDTO {

    /**
     * 促销类型  {@link com.zlp.service.IPromotionKey}
     */
    @NotEmpty(message = "促销类型不能为空")
    private String promoKey;

    /**
     *  促销活动ID
     */
    private String promoId;

    /**
     *  执行步骤路由节点
     */
    private Integer stepKey;

    /**
     *  商品明细信息
     */
    private List<SkuDTO> skuDTOList;

    Integer skuId;

    String reqNo;

    String payType;

    Integer point;

    Integer amount;

}
