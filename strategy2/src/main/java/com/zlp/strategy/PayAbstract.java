package com.zlp.strategy;

import org.springframework.beans.factory.InitializingBean;

/**
 * @description:  抽取共同的策略
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
public abstract class PayAbstract implements InitializingBean {

    /**
     * @description : 共同行为策略模板
     * @param : [stategy]
     * @date : 2020-05-09 12:31
     * @return : java.lang.String 
     */
    public abstract String toPayHtml(String stategy);

    protected Boolean checkParam(String stategy){
        throw new RuntimeException();
    }

}
