package com.zlp.strategy.impl;

import com.zlp.strategy.PayAbstract;
import com.zlp.enums.PayTypeEnum;
import com.zlp.factory.PayFactory;
import org.springframework.stereotype.Service;
/**
 * @description: 支付宝支付方式生成html
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 */
@Service
public class AlipaySategyImpl extends PayAbstract {
  /**
   * @param stategy
   * @return : java.lang.String
   * @description : 共同行为策略模板
   * @date : 2020-05-09 12:31
   */
  @Override
  public String toPayHtml(String stategy) {
    System.out.println("支付宝生成支付html" + stategy);
    return "支付宝生成支付html" + stategy;
  }

  @Override
  public Boolean checkParam(String stategy){
    return Boolean.TRUE;

  }


  @Override
  public void afterPropertiesSet() throws Exception {
    PayFactory.putPayAbstractMap(PayTypeEnum.ALIPAY.getName(), this);
  }

}
