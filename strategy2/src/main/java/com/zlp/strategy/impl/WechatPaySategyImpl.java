package com.zlp.strategy.impl;

import com.zlp.enums.PayTypeEnum;
import com.zlp.factory.PayFactory;
import com.zlp.strategy.PayAbstract;
import org.springframework.stereotype.Service;
/**
 * @description:  微信支付方式生成html
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
@Service
public class WechatPaySategyImpl extends PayAbstract {


    @Override
    public String toPayHtml(String sategy) {
        System.out.println("微信生成支付html"+sategy);
        return "微信生成支付html"+sategy;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        PayFactory.putPayAbstractMap(PayTypeEnum.WECHAT.getName(), this);
    }
}
