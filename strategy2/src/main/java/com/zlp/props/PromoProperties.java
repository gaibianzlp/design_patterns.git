package com.zlp.props;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author: gubin
 * @date: 2022/9/2 15:13
 * @description:
 */
@Component
@Data
//@ConfigurationProperties(prefix = "promo.seckill")
public class PromoProperties implements InitializingBean {

    private Integer buyLimit;
    private String luaDeductStock;
    private String luaBuyLimit;
    private Integer fillTokenBucket;
    private String mqTag;
    private Boolean testing;
    private Integer elapse;
    private String reqnoQueryExpire;

    public static Boolean TESTING;
    public static Integer ELAPSE;
    public static String REQNO_QUERY_EXPIRE;


    @Override
    public void afterPropertiesSet() {
        TESTING = this.testing;
        ELAPSE = this.elapse;
        REQNO_QUERY_EXPIRE = this.reqnoQueryExpire;
    }
}
