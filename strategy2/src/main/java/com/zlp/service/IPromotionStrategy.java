package com.zlp.service;


import com.zlp.chain.Handler;
import com.zlp.dto.PromoInDTO;
import com.zlp.dto.PromoOutDTO;

import java.util.Map;

/**
 * @author: gubin
 * @date: 2022/8/10 14:59
 * @description:
 */
public interface IPromotionStrategy {


    /**
     * 对应的Handler 校验处理器
     */
    Map<Integer, Handler> getHandlers();


    /**
     * 执行的方法process
     */
    PromoOutDTO execute(PromoInDTO promoInDTO);
}
