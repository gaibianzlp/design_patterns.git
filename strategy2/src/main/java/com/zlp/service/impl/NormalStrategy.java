package com.zlp.service.impl;


import com.zlp.chain.Handler;
import com.zlp.dto.PromoContext;
import com.zlp.dto.PromoInDTO;
import com.zlp.dto.PromoOutDTO;
import com.zlp.service.IPromotionKey;
import com.zlp.service.IPromotionStrategy;
import com.zlp.service.PromotionStrategyFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author: gubin
 * @date: 2022/8/10 15:00
 * @description: 普通订单
 */
@Slf4j
@Component
public class NormalStrategy implements IPromotionStrategy, InitializingBean {
    private final Map<Integer, Handler> handlers = new TreeMap<>();

    @Override
    public Map<Integer, Handler> getHandlers() {
        return handlers;
    }

    @Override
    public PromoOutDTO execute(PromoInDTO promoInDTO) {
        try (PromoContext occ = new PromoContext()) {
//            for (Handler handler : handlers) {
//                handler.handle(promoInDTO);
//            }
            handlers.get(promoInDTO.getStepKey()).handle(promoInDTO);
            log.info("无优惠 {}", occ);
            return PromoOutDTO.builder().reqNo(occ.getReqNo()).build();
        }
    }

    @Override
    public void afterPropertiesSet(){
            PromotionStrategyFactory.register(IPromotionKey.NORMAL, this);
    }
}
