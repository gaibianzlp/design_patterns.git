package com.zlp.service;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 单列工厂模式组装对象
 * @author: gubin
 * @date: 2022/8/10 15:03
 * @description:
 */
public class PromotionStrategyFactory {

    private static final Map<String, IPromotionStrategy> PROMOTIONS = new HashMap<>();
    private PromotionStrategyFactory() {
    }

  /**
   * 注册策略策略工厂
   *
   * @see com.zlp.service.impl.SecKillStrategy
   * @see com.zlp.service.impl.NormalStrategy
  */
  public static void register(String code, IPromotionStrategy strategy) {
        if (!StringUtils.isEmpty(code)) {
            PROMOTIONS.put(code, strategy);
        }
    }

    /**
     * 获取活动策略实现类
     */
    public static IPromotionStrategy getPromotionStrategy(String promotionKey) {
        return PROMOTIONS.get(promotionKey);
    }

    /**
     * 获取所有的策略实现类
     */
    public static Set<String> getPromotionKeys() {
        return PROMOTIONS.keySet();
    }



}


