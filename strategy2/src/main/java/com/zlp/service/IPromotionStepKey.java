package com.zlp.service;

/**
 * @author: gubin
 * @date: 2022/8/11 14:08
 * @description:
 */
public interface IPromotionStepKey {
    /**
     * 开始下单
     */
    Integer START = 1;
    /**
     * 订单确认
     */
    Integer CONFIRM = 2;
}
