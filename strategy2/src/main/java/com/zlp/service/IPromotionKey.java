package com.zlp.service;

/**
 * @author: gubin
 * @date: 2022/8/11 14:08
 * @description:
 */
public interface IPromotionKey {
    /**
     * 普通订单
     */
    String NORMAL = "1";
    /**
     * 秒杀订单
     */
    String SECKILL = "2";
}
