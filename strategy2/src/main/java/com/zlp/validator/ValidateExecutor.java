package com.zlp.validator;

import com.zlp.dto.PromoContext;
import com.zlp.dto.PromoInDTO;
import com.zlp.dto.SkuDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

import java.util.Map;

/**
 * 抽象类校验执行器
 * @author: gubin
 * @date: 2022/8/11 16:00
 * @description:
 */
@Slf4j
public abstract class ValidateExecutor {


    /**
     * 获取验证器
     */
    protected abstract Map<Integer, Validator> getValidators();


    /**
     * 验证SKU
     */
    public void verify(SkuDTO skuDTO) {
        final Map<Integer, Validator> validators = getValidators();
        if (CollectionUtils.isEmpty(validators)) {
            return;
        }
        validators.forEach((key, value) -> {
            value.validate(skuDTO);
            log.info("verifyItem validator launch {} {} {}", key, value.getClass().getSimpleName(), PromoContext.get());
        });
    }
    /**
     *  校验促销活动
     */
    public void verify(PromoInDTO promoInDTO) {
        // 获取所有的校验器
        final Map<Integer, Validator> validators = getValidators();
        if (CollectionUtils.isEmpty(validators)) {
            return;
        }
        validators.forEach((key, value) -> {
            value.validate(promoInDTO);
            log.info("verify validator launch {} {} {}", key, value.getClass().getSimpleName(), PromoContext.get());
        });
    }

}
