//package com.zlp.validator.impl;
//
//import com.alibaba.fastjson.JSONObject;
//import com.volvo.mall.promotion.chain.Handler;
//import com.volvo.mall.promotion.dto.PromoContext;
//import com.volvo.mall.promotion.dto.PromoInDTO;
//import com.volvo.mall.promotion.dto.PushMQDTO;
//import com.volvo.mall.promotion.dto.SkuDTO;
//import com.volvo.mall.promotion.exception.ErrorCode;
//import com.volvo.mall.promotion.exception.PromoProducterException;
//import com.volvo.mall.promotion.mq.producer.PromoQueueProducer;
//import com.volvo.mall.promotion.validator.Validator;
//import com.zlp.dto.PromoInDTO;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.stereotype.Component;
//
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//
///**
// * @author: gubin
// * @date: 2022/8/11 16:08
// * @description:
// */
//@Slf4j
//@Component
//public class PushMQValidator extends Validator implements InitializingBean {
//
//    @Autowired
//    private PromoQueueProducer promoQueueProducer;
//    @Autowired
//    private Handler confirmHandler;
//    @Autowired
//    private StringRedisTemplate redisTemplate;
//    @Autowired
//    ApplicationContext applicationContext;
//
//    @Override
//    public void afterPropertiesSet() {
//        confirmHandler.getValidators().put(3, this);
//    }
//
//    @Override
//    public void validate(SkuDTO skuDTO) {
//        int skuId = skuDTO.getId();
//        PromoContext occ = PromoContext.get();
//        String memberId = occ.getMemberId();
//        int quantity = skuDTO.getQuantity();
////        String reqNo = occ.getReqNo();
////        Long result = redisTemplate.opsForHash().delete(PromoConst.seckillReqnoKey, reqNo);
////        if (result != 1) {
////            log.info("reqNo已失效: reqNo {} result {}", reqNo, result);
////            throw new CustomException(ErrorEnum.REQNO_NOT_EXIST_ERROR);
////        }
//        // 压入队列待消费
//        PushMQDTO pushMQDTO = PushMQDTO.builder()
//                .promoId(occ.getPromoId())
//                .reqNo(occ.getReqNo())
//                .memberId(memberId)
//                .skuId(skuId)
//                .quantity(quantity)
//                .payType(occ.getPayType())
//                .point(occ.getPoint())
//                .amount(occ.getAmount())
//                .promoKey(occ.getPromoKey())
//                .createTime(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()))
//                .build();
//        log.info("压入队列待消费: {}", JSONObject.toJSONString(pushMQDTO));
//        try {
//            log.info("validate秒杀信息开始发送mq");
//            promoQueueProducer.push(pushMQDTO);
//        }catch (Exception e){
//            log.error("validate秒杀发送mq异常信息 message:{}", e.getMessage(),e);
////            throw new PromoProducterException(ErrorCode.SEND_MQ_FAIL);
//        }
//    }
//
//    @Override
//    public void validate(PromoInDTO promoInDTO) {
//    }
//}
