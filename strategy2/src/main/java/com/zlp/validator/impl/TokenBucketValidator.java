package com.zlp.validator.impl;


import com.zlp.chain.Handler;
import com.zlp.dto.PromoInDTO;
import com.zlp.dto.SkuDTO;
import com.zlp.validator.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author: gubin
 * @date: 2022/9/2 14:30
 * @description:
 */
@Slf4j
@Component
public class TokenBucketValidator extends Validator implements InitializingBean {

    @Autowired
    private Handler startHandler;

    @Override
    public void afterPropertiesSet() {
//        startHandler.getValidators().add(this);
    }


    @Override
    public void validate(PromoInDTO promoInDTO) {
//        String token = redisTemplate.opsForList().rightPop(PromoConst.seckillTokenKey);
//        if (token == null) {
//            throw new CustomException(ErrorEnum.TOKEN_BUCKET_EMPTY_ERROR);
//        }
    }
}
