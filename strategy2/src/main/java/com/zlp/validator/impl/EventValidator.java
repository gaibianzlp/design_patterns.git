package com.zlp.validator.impl;


import com.alibaba.fastjson.JSON;
import com.zlp.chain.Handler;
import com.zlp.dto.PromoEventDTO;
import com.zlp.dto.PromoInDTO;
import com.zlp.dto.SkuDTO;
import com.zlp.validator.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author: gubin
 * @date: 2022/8/11 16:08
 * @description:
 */
@Slf4j
@Component
public class EventValidator extends Validator  {


    @Resource
    private Handler startHandler;
    @Resource
    private Handler confirmHandler;

    @Override
    public void afterPropertiesSet() {
        startHandler.getValidators().put(1, this);
        confirmHandler.getValidators().put(1, this);
    }




    @Override
    public void validate(PromoInDTO promoInDTO) {
    log.info("promoInDTO={}", JSON.toJSONString(promoInDTO));
        SkuDTO skuDTO = new SkuDTO();
        promoInDTO.setPromoId(skuDTO.getPromoId());
        // 填充redis中的sku限购数到入参sku对象
        promoInDTO.getSkuDTOList().forEach(s -> {
            if (promoInDTO.getSkuId().equals(s.getId())) {
                s.setLimit(skuDTO.getLimit());
            }
        });

        PromoEventDTO eventDTO = new PromoEventDTO();
        Date dt = new Date();

    }

}
