//package com.zlp.validator.impl;
//
//import com.volvo.mall.common.enums.ErrorEnum;
//import com.volvo.mall.common.exception.CustomException;
//import com.volvo.mall.promotion.chain.Handler;
//import com.volvo.mall.promotion.config.props.PromoProperties;
//import com.volvo.mall.promotion.consts.PromoConst;
//import com.volvo.mall.promotion.dto.PromoContext;
//import com.volvo.mall.promotion.dto.PromoInDTO;
//import com.volvo.mall.promotion.dto.SkuDTO;
//import com.volvo.mall.promotion.validator.Validator;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.data.redis.core.script.DefaultRedisScript;
//import org.springframework.stereotype.Component;
//import org.springframework.util.ObjectUtils;
//
//import java.util.Arrays;
//
///**
// * @author: gubin
// * @date: 2022/8/11 16:08
// * @description:
// */
//@Slf4j
//@Component
//public class RepeatValidator extends Validator implements InitializingBean {
//
//    @Autowired
//    private StringRedisTemplate redisTemplate;
//    @Autowired
//    private PromoProperties promoProperties;
//    @Autowired
//    private Handler startHandler;
//
//    @Override
//    public void afterPropertiesSet() {
//        startHandler.getValidators().put(2, this);
//    }
//
//    @Override
//    public void validate(SkuDTO skuDTO) {
//        Integer skuId = skuDTO.getId();
//        PromoContext occ = PromoContext.get();
//        String memberId = occ.getMemberId();
//        String keyStock = PromoConst.seckillStockKey + "{" + skuId + "}";
//        String keyLimit = PromoConst.seckillLimitKey + "{" + skuId + "}" + "-" + memberId;
////                + "-" + DateTimeFormatter.ofPattern("MMdd").format(LocalDateTime.now());
//        DefaultRedisScript<Long> defaultRedisScript = new DefaultRedisScript<>();
//        defaultRedisScript.setResultType(Long.class);
//        defaultRedisScript.setScriptText(promoProperties.getLuaBuyLimit());
//        Long stock = redisTemplate.execute(defaultRedisScript, Arrays.asList(keyStock, keyLimit), skuDTO.getLimit() + "");
//        log.info("RepeatValidator validate stock:{}",stock);
//        if (ObjectUtils.isEmpty(stock)) {
//            throw new CustomException(ErrorEnum.VERIFY_STOCK_ERROR);
//        }
//        if (stock == -1) {
//            log.info("{}：已售罄", skuId);
//            throw new CustomException(ErrorEnum.SOLD_OUT_ERROR);
//        } else if (stock == -2) {
//            log.info("memberId: {}, skuId: {} 购买超限", memberId, skuId);
//            throw new CustomException(ErrorEnum.REPEAT_BUY_ERROR);
//        }
//    }
//
//    @Override
//    public void validate(PromoInDTO promoInDTO) {
//    }
//}
