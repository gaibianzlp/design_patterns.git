package com.zlp.validator;


import com.zlp.dto.PromoInDTO;
import com.zlp.dto.SkuDTO;
import org.springframework.beans.factory.InitializingBean;

/**
 *
 *  校验器
 * @author: gubin
 * @date: 2022/8/11 16:00
 * @description:
 */
public abstract class Validator implements InitializingBean {

    /**
     *  sku校验
     */
    protected  void validate(SkuDTO skuDTO){

    }

    /**
     *  活动校验
     */
    public abstract void validate(PromoInDTO promoInDTO);
}
