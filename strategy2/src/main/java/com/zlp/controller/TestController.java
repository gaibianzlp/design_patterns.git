package com.zlp.controller;

import com.alibaba.fastjson.JSONObject;
import com.zlp.context.PayStrategyContext;
import com.zlp.dto.PromoInDTO;
import com.zlp.dto.PromoOutDTO;
import com.zlp.dto.SkuDTO;
import com.zlp.enums.PayTypeEnum;
import com.zlp.service.IPromotionStepKey;
import com.zlp.service.IPromotionStrategy;
import com.zlp.service.PromotionStrategyFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;

/**
 * @description: PayController
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 **/
@Slf4j
@RestController
public class TestController {


    @PostMapping(value = "/api/order/promo/start")
    public PromoOutDTO promoStart(@RequestBody @Valid PromoInDTO promoInDTO) {

        log.info("OrderAPI promoStart 促销活动开始入参promoInDTO:{}", JSONObject.toJSONString(promoInDTO));
        promoInDTO.setStepKey(IPromotionStepKey.START);
        int quantity = 1;
        promoInDTO.setSkuDTOList(Arrays.asList(
                SkuDTO.builder().
                        id(3).
                        quantity(quantity).
                        limit(1).
                        spuName("测试").
                        stock(5).
                        build()));
        IPromotionStrategy strategy = PromotionStrategyFactory.getPromotionStrategy(promoInDTO.getPromoKey());
        PromoOutDTO promoOutDTO = strategy.execute(promoInDTO);

        return promoOutDTO;
    }
}
