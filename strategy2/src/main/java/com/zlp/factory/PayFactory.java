package com.zlp.factory;

import com.zlp.strategy.PayAbstract;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @description: 工厂模式调用
 * @author: LiPing.Zou
 * @create: 2020-05-08 21:43
 */
public class PayFactory {

  public static Map<String, PayAbstract> payAbstractMap = new HashMap<>();

  /** 添加具体策略对象bean */
  public static void putPayAbstractMap(String name, PayAbstract payAbstract) {
    payAbstractMap.put(name, payAbstract);
  }

  public static PayAbstract invokePay(String name) {

    if (Objects.nonNull(payAbstractMap.get(name))) {
      return payAbstractMap.get(name);
    }
    return null;
  }
}
