package com.zlp.chain;

import com.zlp.dto.PromoInDTO;
import com.zlp.validator.ValidateExecutor;
import com.zlp.validator.Validator;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;
import java.util.TreeMap;


/**
 *
 * 装载Validator 抽象模板工厂
 * @author: gubin
 * @date: 2022/8/10 15:46
 * @description:
 */
public abstract class Handler extends ValidateExecutor implements InitializingBean {


    /**
     * 校验器
     */
    private final Map<Integer, Validator> validators = new TreeMap<>();

    /**
     * Bean实例化填充进去
     * 获取所有的validators
     */
    @Override
    public Map<Integer, Validator> getValidators() {
        return this.validators;
    }

    /**
     *  处理器
     */
    public abstract void handle(PromoInDTO promoInDTO);


}
