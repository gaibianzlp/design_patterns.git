package com.zlp.chain;

import com.alibaba.fastjson.JSONObject;

import com.zlp.dto.PromoContext;
import com.zlp.dto.PromoInDTO;
import com.zlp.service.IPromotionStepKey;
import com.zlp.service.IPromotionStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author: gubin
 * @date: 2022/8/10 15:46
 * @description:
 */
@Slf4j
@Component
public class StartHandler extends Handler {

    @Autowired
    IPromotionStrategy secKillStrategy;

    @Override
    public void afterPropertiesSet() {
        secKillStrategy.getHandlers().put(IPromotionStepKey.START, this);
    }

    @Override
    public void handle(PromoInDTO promoInDTO) {
        log.info("StartHandler 开始handle请求入参promoInDTO:{}", JSONObject.toJSONString(promoInDTO));
        PromoContext occ = PromoContext.get();

        // 执行父类 ValidateExecutor.verify
        super.verify(promoInDTO);
        // 校验SKU信息
        promoInDTO.getSkuDTOList().forEach(this::verify);

        log.info("sku库存和限购校验成功 {}", occ);
    }

}
