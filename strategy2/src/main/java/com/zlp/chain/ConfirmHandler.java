package com.zlp.chain;


import com.alibaba.fastjson.JSON;
import com.zlp.dto.PromoContext;
import com.zlp.dto.PromoInDTO;
import com.zlp.service.IPromotionStepKey;
import com.zlp.service.IPromotionStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**
 * @author: gubin
 * @date: 2022/8/10 15:46
 * @description:
 */
@Slf4j
@Component
public class ConfirmHandler extends Handler {

    @Autowired
    IPromotionStrategy secKillStrategy;
    @Autowired
    IPromotionStrategy normalStrategy;

    @Override
    public void afterPropertiesSet() {
        secKillStrategy.getHandlers().put(IPromotionStepKey.CONFIRM, this);
    }

    @Override
    public void handle(PromoInDTO promoInDTO) {
    log.info("ConfirmHandler 确认handle请求入参promoInDTO:{}", JSON.toJSONString(promoInDTO));
        PromoContext occ = PromoContext.get();
        occ.setReqNo(promoInDTO.getReqNo());
        occ.setPayType(promoInDTO.getPayType());
        occ.setPoint(promoInDTO.getPoint());
        occ.setAmount(promoInDTO.getAmount());

        verify(promoInDTO);
        promoInDTO.getSkuDTOList().forEach(this::verify);

        log.info("确认订单支付方式 {}", occ);
    }

}
