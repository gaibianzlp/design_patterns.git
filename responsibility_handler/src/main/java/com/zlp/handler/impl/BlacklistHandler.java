package com.zlp.handler.impl;

import com.zlp.handler.GetWayHandler;

/**
 * @description: 黑名单处理者
 * @author: LiPing.Zou
 * @create: 2020-05-09 15:38
 **/
public class BlacklistHandler extends GetWayHandler {

//    private ConversationHandler conversationHandler;



    @Override
    public void execute() {
        System.out.println("第一步黑名单处理者");
        setNextExecute();
    }

   /* public void setNextGetWayHandler(ConversationHandler conversationHandler){
        this.conversationHandler = conversationHandler;
    }*/
}
