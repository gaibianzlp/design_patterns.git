package com.zlp.handler.impl;

import com.zlp.handler.GetWayHandler;

/**
 * @description: 会话验证
 * @author: LiPing.Zou
 * @create: 2020-05-09 15:40
 **/
public class ConversationHandler  extends GetWayHandler {

//    private CurrentLimitHandler currentLimitHandler;

    @Override
    public void execute() {
        System.out.println("第二步黑名单处理者");

//        currentLimitHandler.execute();
        setNextExecute();

    }

   /* public void  setNextGetWayHandler(CurrentLimitHandler currentLimitHandler){
        this.currentLimitHandler = currentLimitHandler;
    }*/

}
