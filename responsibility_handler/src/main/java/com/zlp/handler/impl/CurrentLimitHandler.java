package com.zlp.handler.impl;

import com.zlp.handler.GetWayHandler;

/**
 * @description: 限流
 * @author: LiPing.Zou
 * @create: 2020-05-09 15:43
 **/
public class CurrentLimitHandler extends GetWayHandler {



    @Override
    public void execute() {
        System.out.println("第三步执行限流Handler");
    }
}
