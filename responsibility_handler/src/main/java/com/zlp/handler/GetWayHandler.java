package com.zlp.handler;

import java.util.Objects;

/**
 * @description: 把共同行为抽取出来GetWayHandler
 * @author: LiPing.Zou
 * @create: 2020-05-09 15:34
 **/
public abstract class GetWayHandler {

    protected GetWayHandler nextGetWayHandler;

    /** 
     *  执行方法
     * @date: 2020-05-09 17:18
     * @return: void 
     */
    public abstract void execute();


    /**
     *  执行下一个execute
     * @date: 2020-05-09 17:18
     * @return: void
     */
    protected void setNextExecute(){
        if (Objects.nonNull(nextGetWayHandler)){
            nextGetWayHandler.execute();
        }
    }

    /**
     *  设置下一个 setNextGetWayHandler
     * @date: 2020-05-09 17:18
     * @return: void
     */
    public void setNextGetWayHandler(GetWayHandler nextGetWayHandler){
        this.nextGetWayHandler = nextGetWayHandler;
    }
}
