package com.zlp.factory;

import com.zlp.handler.GetWayHandler;
import com.zlp.handler.impl.BlacklistHandler;
import com.zlp.handler.impl.ConversationHandler;
import com.zlp.handler.impl.CurrentLimitHandler;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 16:28
 **/
public class HandlerFactory {


    public static GetWayHandler executueHandler(){

        BlacklistHandler blacklistHandler = new BlacklistHandler();
        ConversationHandler conversationHandler = new ConversationHandler();
        blacklistHandler.setNextGetWayHandler(conversationHandler);
        CurrentLimitHandler currentLimitHandler = new CurrentLimitHandler();
        conversationHandler.setNextGetWayHandler(currentLimitHandler);
        return blacklistHandler;
    }


}
