package com.zlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:  Handler启动类
 * @author: LiPing.Zou
 * @create: 2020-05-09 16:56
 **/
@SpringBootApplication
public class HandlerApp {

    public static void main(String[] args) {
        SpringApplication.run(HandlerApp.class,args);
    }
}
