package com.zlp.design.visitor;

public interface Animal {

    void accept(Person person);
}