package com.zlp.design.adapter;

public class Dog extends Animal {

    public String read(){
        return eat();
    }

    public static void main(String[] args) {

        Dog dog = new Dog();
        System.out.println(dog.read());

    }

    @Override
    protected String speak() {
        return null;
    }
}
