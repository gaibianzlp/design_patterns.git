package com.zlp.design.adapter.v1;

//TF卡接口
public interface TFCard {

    //读取TF卡方法
    String readTF();


    //写入TF卡功能
    void writeTF(String msg);
}