package com.zlp.design.proxy.statics;

/**
 * 测试类
 * @date: 2022/3/8 10:59
 */
public class Client {

    public static void main(String[] args) {
        ProxyPoint pp = new ProxyPoint();
        pp.sell();
    }
}