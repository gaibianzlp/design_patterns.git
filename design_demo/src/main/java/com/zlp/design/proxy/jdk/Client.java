package com.zlp.design.proxy.jdk;


/**
 * 测试类
 * @date: 2022/3/8 10:59
 */
public class Client {

    public static void main(String[] args) {
        ProxyFactory proxyFactory = new ProxyFactory();
        SellTickets proxyObject = proxyFactory.getProxyObject();


        proxyObject.sell();
//        proxyObject.toString();
        TrainStation trainStation = (TrainStation)proxyObject;
        trainStation.toString();

    }
}