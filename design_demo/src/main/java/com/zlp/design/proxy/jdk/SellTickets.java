package com.zlp.design.proxy.jdk;

/**
 * 卖票接口
 * @date: 2022/3/8 10:59
 */
public interface SellTickets {

    void sell();
}