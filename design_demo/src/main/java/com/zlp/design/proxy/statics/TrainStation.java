package com.zlp.design.proxy.statics;

/**
 * 火车站  火车站具有卖票功能，所以需要实现SellTickets接口
 * @date: 2022/3/8 10:59
 */
public class TrainStation implements SellTickets {

    public void sell() {
        System.out.println("火车站卖票");
    }
}