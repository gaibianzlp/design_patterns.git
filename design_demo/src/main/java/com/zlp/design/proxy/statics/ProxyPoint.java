package com.zlp.design.proxy.statics;

/**
 * 代售点
 * @date: 2022/3/8 10:59
 */
public class ProxyPoint implements SellTickets {

    private TrainStation station = new TrainStation();

    public void sell() {
        System.out.println("代理点收取一些服务费用");
        station.sell();
    }
}