package com.zlp.design.interpret.test;


/**
 * 终结符表达式角色 变量表达式
 * @date: 2022/3/14 14:00
 * @return:
 */
public class Variable<T> extends AbstractExpression<T> {

    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public T interpret(Context ctx) {
        return (T) ctx.getValue(this);
    }

    @Override
    public String toString() {
        return name;
    }
}