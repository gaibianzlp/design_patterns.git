package com.zlp.design.interpret;

/**
 * 终结符表达式角色 变量表达式
 * @date: 2022/3/14 14:00
 * @return:
 */
public class Variable extends AbstractExpression {

    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public int interpret(Context ctx) {
        return ctx.getValue(this);
    }

    @Override
    public String toString() {
        return name;
    }
}