package com.zlp.design.interpret.test;



/**
 * 非终结符表达式角色  加法表达式
 * @date: 2022/3/14 14:00
 * @return:
 */
public class Plus<T> extends AbstractExpression<Integer> {

    private AbstractExpression left;
    private AbstractExpression right;

    public Plus(AbstractExpression left, AbstractExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Integer interpret(Context context) {

        return (int)left.interpret(context) + (int)right.interpret(context);
    }

    @Override
    public String toString() {
        return "(" + left.toString() + " + " + right.toString() + ")";
    }
}
