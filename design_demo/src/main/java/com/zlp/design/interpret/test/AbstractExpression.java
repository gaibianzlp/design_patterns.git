package com.zlp.design.interpret.test;


/**
 * AbstractExpression
 * 抽象角色
 * @date: 2022/3/14 14:00
 */
public abstract class AbstractExpression<T> {

    public abstract T interpret(Context context);
}