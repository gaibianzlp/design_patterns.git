package com.zlp.design.interpret;

/**
 * AbstractExpression
 * 抽象角色
 * @date: 2022/3/14 14:00
 */
public abstract class AbstractExpression {

    public abstract int interpret(Context context);
}