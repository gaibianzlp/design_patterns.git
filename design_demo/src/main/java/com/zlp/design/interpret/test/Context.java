package com.zlp.design.interpret.test;


import java.util.HashMap;
import java.util.Map;

/**
 * 环境类
 * @date: 2022/3/14 14:00
 */
public class Context<T> {

    private Map<Variable, T> map = new HashMap<>();

    public void assign(Variable var, T value) {
        map.put(var, value);
    }


    public T getValue(Variable var) {
        T value = map.get(var);
        return value;
    }
}