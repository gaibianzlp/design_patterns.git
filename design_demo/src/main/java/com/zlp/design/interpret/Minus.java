package com.zlp.design.interpret;

/**
 * 非终结符表达式角色 减法表达式
 * @date: 2022/3/14 14:00
 * @return:
 */
public class Minus extends AbstractExpression {

    private AbstractExpression left;
    private AbstractExpression right;

    public Minus(AbstractExpression left, AbstractExpression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int interpret(Context context) {
        return left.interpret(context) - right.interpret(context);
    }

    @Override
    public String toString() {
        return "(" + left.toString() + " - " + right.toString() + ")";
    }
}