package com.zlp.design.factory.statics;


/**
 * 咖啡店
 * @date: 2022/3/7 19:48
 * @return:
 */
public class CoffeeStore {

    public static Coffee orderCoffee(String orderType){
        Coffee coffee = SimpleCoffeeFactory.createCoffee(orderType);
        System.out.println(coffee.getName());
        // 添加配料
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
