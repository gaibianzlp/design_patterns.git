package com.zlp.design.factory.abstracts;

/**
 * 意大利风味甜点工厂 (生产拿铁咖啡和提拉米苏甜点)
 * @date: 2022/3/7 17:50
 */
public class ItalyDessertFactory implements DessertFactory {

    public Coffee createCoffee() {
        return new LatteCoffee();
    }
    public Dessert createDessert() {
        return new Tiramisu();
    }
}