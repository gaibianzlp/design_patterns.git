package com.zlp.design.factory.abstracts;


/**
 * 抽象工厂
 * @date: 2022/3/7 17:45
 */
public interface DessertFactory {

    /**
     * 生产 coffee 功能
     * @date: 2022/3/7 17:47
     * @return: com.zlp.design.factory.abstracts.Coffee
     */
    Coffee createCoffee();

    /**
     * 生产 dessert 功能
     * @date: 2022/3/7 17:47
     * @return: com.zlp.design.factory.abstracts.Coffee
     */
    Dessert createDessert();
}