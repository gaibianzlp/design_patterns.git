package com.zlp.design.factory.config;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 咖啡工厂
 * @date: 2022/3/8 10:05
 */
public class CoffeeFactory {

    private CoffeeFactory() {}

    private static Map<String, Coffee> map;

    static {
        map = new HashMap<>();
        Properties p = new Properties();
        InputStream is = CoffeeFactory.class.getClassLoader().getResourceAsStream("bean.properties");
        try {
            p.load(is);
            //遍历Properties集合对象
            Set<Object> keys = p.keySet();
            for (Object key : keys) {
                //根据键获取值（全类名）
                String className = p.getProperty((String) key);
                //获取字节码对象
                Class<Coffee> clazz = (Class<Coffee>) Class.forName(className);
                Coffee obj = clazz.newInstance();
                map.put((String) key, obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 创建咖啡
     * @date: 2022/3/8 10:14
     * @return: com.zlp.design.factory.config.Coffee
     */
    public static Coffee createCoffee(String name) {
        return map.get(name);
    }


}