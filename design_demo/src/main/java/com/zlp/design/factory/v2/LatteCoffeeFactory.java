package com.zlp.design.factory.v2;

/**
 * 拿铁咖啡工厂
 * @date: 2022/3/7 20:05
 * @return:
 */
public class LatteCoffeeFactory implements CoffeeFactory {

    public Coffee createCoffee() {
        return new LatteCoffee();
    }
}