package com.zlp.design.factory.simple;


/**
 * 简单工厂调用对象
 * @date: 2022/3/7 18:34
 */
public class SimpleCoffeeFactory {

    public  Coffee createCoffee(String orderType) {
        Coffee coffee = null;
        if ("american".equals(orderType)) {
            coffee = new AmericanCoffee();
        } else if ("latte".equals(orderType)) {
            coffee = new LatteCoffee();
        }else{
            System.out.println("您点的咖啡暂时还没有这种类型...");
        }
        return coffee;
    }
}
