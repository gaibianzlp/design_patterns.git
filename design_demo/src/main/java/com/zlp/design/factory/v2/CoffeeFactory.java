package com.zlp.design.factory.v2;


/**
 * 抽象工厂
 * @date: 2022/3/7 20:04
 * @return:
 */
public interface CoffeeFactory {

    Coffee createCoffee();
}