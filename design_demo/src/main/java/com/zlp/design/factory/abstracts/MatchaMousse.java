package com.zlp.design.factory.abstracts;


/**
 * 抹茶慕斯甜点
 * @date: 2022/3/7 17:47
 */
public class MatchaMousse extends Dessert {


    @Override
    protected void show() {
        System.out.println("抹茶慕斯甜点");
    }
}
