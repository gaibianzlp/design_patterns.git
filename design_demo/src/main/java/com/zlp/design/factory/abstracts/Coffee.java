package com.zlp.design.factory.abstracts;

public abstract class Coffee {

    protected abstract String getName();
    /**
     * 添加牛奶
     * @date: 2022/3/7 9:46
     */
    public void addMilk(){
        System.out.println("咖啡，添加牛奶");
    }

    /**
     * 添加糖
     * @date: 2022/3/7 9:46
     */
    public void addSugar(){
        System.out.println("咖啡，糖");
    }
}
