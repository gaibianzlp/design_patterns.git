package com.zlp.design.factory.v1;

/**
 * 美式咖啡
 * @date: 2022/3/7 18:33
 */
public class AmericanCoffee extends Coffee {

    @Override
    protected String getName() {
        return "美式咖啡";
    }
}
