package com.zlp.design.factory.abstracts;


/**
 * 甜点
 * @date: 2022/3/7 17:46
 */
public abstract class Dessert {

    protected abstract void show();

}
