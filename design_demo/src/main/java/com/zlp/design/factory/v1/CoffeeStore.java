package com.zlp.design.factory.v1;

public class CoffeeStore {



    public static Coffee orderCoffee(String orderType){
        Coffee coffee = null;
        if ("american".equals(orderType)) {
            coffee = new AmericanCoffee();
        } else if ("latte".equals(orderType)) {
            coffee = new LatteCoffee();
        }else{
            System.out.println("您点的咖啡暂时还没有这种类型...");
        }
        // 添加配料
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
