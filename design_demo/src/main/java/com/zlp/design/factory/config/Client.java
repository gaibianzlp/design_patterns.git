package com.zlp.design.factory.config;

public class Client {

    public static void main(String[] args) {
        CoffeeStore coffeeStore = new CoffeeStore();
        Coffee american = coffeeStore.orderCoffee("american");
        System.out.println(american.getName());

    }
}
