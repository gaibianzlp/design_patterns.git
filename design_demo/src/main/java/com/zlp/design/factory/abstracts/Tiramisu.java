package com.zlp.design.factory.abstracts;


/**
 * 提拉米苏甜点
 * @date: 2022/3/7 17:47
 */
public class Tiramisu extends Dessert {

    @Override
    protected void show() {
        System.out.println("提拉米苏甜点");
    }
}
