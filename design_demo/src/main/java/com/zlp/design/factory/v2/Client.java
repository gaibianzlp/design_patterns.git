package com.zlp.design.factory.v2;

public class Client {

    public static void main(String[] args) {
        CoffeeFactory factory = new AmericanCoffeeFactory();
        CoffeeStore coffeeStore = new CoffeeStore(factory);
        Coffee american = coffeeStore.orderCoffee();
        System.out.println(american.getName());
    }
}
