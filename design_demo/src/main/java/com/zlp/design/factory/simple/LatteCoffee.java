package com.zlp.design.factory.simple;
/**
 * 拿铁咖啡
 * @date: 2022/3/7 18:33
 */
public class LatteCoffee extends Coffee{


    @Override
    protected String getName() {
        return "拿铁咖啡";
    }
}
