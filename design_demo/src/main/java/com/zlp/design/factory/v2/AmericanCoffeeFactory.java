package com.zlp.design.factory.v2;

/**
 * 美式咖啡工厂
 * @date: 2022/3/7 20:04
 */
public class AmericanCoffeeFactory implements CoffeeFactory {

    public Coffee createCoffee() {
        return new AmericanCoffee();
    }
}