package com.zlp.design.factory.abstracts;

public class AmericanCoffee extends Coffee {

    @Override
    protected String getName() {
        return "美式咖啡";
    }
}
