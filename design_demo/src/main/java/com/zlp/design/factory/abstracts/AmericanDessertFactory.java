package com.zlp.design.factory.abstracts;


/**
 * 美式甜点工厂（生产美式咖啡和抹茶慕斯甜点）
 * @date: 2022/3/7 13:35
 */
public class AmericanDessertFactory implements DessertFactory {

    public Coffee createCoffee() {
        return new AmericanCoffee();
    }

    public Dessert createDessert() {
        return new MatchaMousse();
    }
}