package com.zlp.design.factory.v2;


public class AmericanCoffee extends Coffee {

    @Override
    protected String getName() {
        return "美式咖啡";
    }
}
