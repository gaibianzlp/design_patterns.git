package com.zlp.design.factory.abstracts;

public class client {

    public static void main(String[] args) {

        // 意大利风味甜点工厂
        ItalyDessertFactory italyDessertFactory = new ItalyDessertFactory();
        Coffee coffee = italyDessertFactory.createCoffee();
        coffee.addMilk();
        coffee.addSugar();
        System.out.println(coffee.getName());
        // 提拉米苏甜点
        Dessert dessert = italyDessertFactory.createDessert();
        dessert.show();

    }
}
