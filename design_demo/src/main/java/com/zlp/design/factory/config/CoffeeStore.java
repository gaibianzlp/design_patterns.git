package com.zlp.design.factory.config;

public class CoffeeStore {



    public static Coffee orderCoffee(String orderType){
        Coffee coffee = CoffeeFactory.createCoffee(orderType);
        coffee.addMilk();
        coffee.addSugar();
        return coffee;
    }
}
