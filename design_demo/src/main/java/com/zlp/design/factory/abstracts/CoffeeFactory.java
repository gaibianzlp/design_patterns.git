package com.zlp.design.factory.abstracts;

public interface CoffeeFactory {

    Coffee createCoffee();
}