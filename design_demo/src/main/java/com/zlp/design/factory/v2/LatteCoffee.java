package com.zlp.design.factory.v2;

import com.zlp.design.factory.v2.Coffee;

public class LatteCoffee extends Coffee{


    @Override
    protected String getName() {
        return "拿铁咖啡";
    }
}
