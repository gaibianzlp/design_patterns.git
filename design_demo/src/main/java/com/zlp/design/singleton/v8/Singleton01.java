package com.zlp.design.singleton.v8;

import java.io.Serializable;

/**
 * 静态内部类方式
 */
public class Singleton01 implements Serializable {

    //私有构造方法
    private Singleton01() {}

    private static class SingletonHolder {
        private static final Singleton01 INSTANCE = new Singleton01();
    }

    //对外提供静态方法获取该对象
    public static Singleton01 getInstance() {
        return SingletonHolder.INSTANCE;
    }
}