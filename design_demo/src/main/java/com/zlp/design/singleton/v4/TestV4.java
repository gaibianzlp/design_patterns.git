package com.zlp.design.singleton.v4;

import com.zlp.design.singleton.v1.Singleton;

public class TestV4 {

    public static void main(String[] args) {

        Singleton instance = Singleton.getInstance();
        Singleton instance1 = Singleton.getInstance();
        System.out.println(instance == instance1);

    }
}
