package com.zlp.design.singleton.v7;


public class TestV7 {

    public static void main(String[] args) {

        Singleton instance = Singleton.INSTANCE;
        Singleton instance1 = Singleton.INSTANCE;
        System.out.println(instance == instance1);

    }
}
