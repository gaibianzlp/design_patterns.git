package com.zlp.design.singleton.v7;

/**
 * 枚举方式
 */
public enum Singleton {
    INSTANCE;
}