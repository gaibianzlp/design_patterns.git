package com.zlp.design.singleton.v6;

/**
 * 双重检查方式
 */
public class Singleton01 {

    //私有构造方法
    private Singleton01() {
         /*
           反射破解单例模式需要添加的代码
        */
        if(instance != null) {
            throw new RuntimeException();
        }
    }

    private static volatile Singleton01 instance;

    //对外提供静态方法获取该对象
    public static Singleton01 getInstance() {
        //第一次判断，如果instance不为null，不进入抢锁阶段，直接返回实际
        if (instance == null) {
            synchronized (Singleton01.class) {
                //抢到锁之后再次判断是否为空
                if (instance == null) {
                    instance = new Singleton01();
                }
            }
        }
        return instance;
    }
}