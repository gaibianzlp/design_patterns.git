package com.zlp.design.singleton.v2;

import com.zlp.design.singleton.v1.Singleton;

public class TestV2 {

    public static void main(String[] args) {

        Singleton instance = Singleton.getInstance();
        Singleton instance1 = Singleton.getInstance();
        System.out.println(instance == instance1);

    }
}
