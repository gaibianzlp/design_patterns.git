package com.zlp.design.flyweight.v1;

public class User { //外部状态
	private String name;
	public User(String name) {
		super();
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
