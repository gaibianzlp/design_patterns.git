package com.zlp.design.flyweight.source;

public class Demo1 {

    public static void main(String[] args) {
        Integer i1 = Integer.valueOf((int)127);
        Integer i2 = Integer.valueOf((int)127);
        System.out.println(new StringBuilder()
                .append("i1\u548ci2\u5bf9\u8c61\u662f\u5426\u662f\u540c\u4e00\u4e2a\u5bf9\u8c61\uff1f")
                .append((i1 == i2)).toString());
        Integer i3 = Integer.valueOf((int)128);
        Integer i4 = Integer.valueOf((int)128);
        System.out.println(new StringBuilder()
                .append("i3\u548ci4\u5bf9\u8c61\u662f\u5426\u662f\u540c\u4e00\u4e2a\u5bf9\u8c61\uff1f")
                .append((i3 == i4)).toString());
    }
}
