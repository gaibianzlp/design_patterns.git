package com.zlp.design.flyweight;


/**
 * 具体享元角色
 * @date: 2022/3/9 16:43
 */
public class OBox extends AbstractBox {


    @Override
    public String getShape() {
        return "O";
    }
}