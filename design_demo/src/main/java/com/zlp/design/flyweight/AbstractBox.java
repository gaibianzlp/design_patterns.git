package com.zlp.design.flyweight;


/**
 * 抽象享元角色
 * @date: 2022/3/9 16:41
 */
public abstract class AbstractBox {

    /**
     * 获取图片形状
     * @date: 2022/3/9 16:41
     */
    public abstract String getShape();


    /**
     * 颜色
     * @date: 2022/3/9 16:41
     */
    public void display(String color) {
        System.out.println("方块形状：" + this.getShape() + " 颜色：" + color);
    }
}