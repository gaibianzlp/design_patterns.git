package com.zlp.design.flyweight;

public class Client {

    public static void main(String[] args) {

        BoxFactory boxFactory = BoxFactory.getInstance();
        AbstractBox box = boxFactory.getBox("I");
        box.display("black");
    }
}
