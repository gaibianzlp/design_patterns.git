package com.zlp.design.flyweight;

import java.util.HashMap;


/**
 * 单例工厂（饿汉式）
 * @date: 2022/3/9 16:47
 */
public class BoxFactory {

    private static HashMap<String, AbstractBox> map;


    private BoxFactory() {
        map = new HashMap<>();
        AbstractBox iBox = new IBox();
        AbstractBox lBox = new LBox();
        AbstractBox oBox = new OBox();
        map.put("I", iBox);
        map.put("L", lBox);
        map.put("O", oBox);
    }


    private static class SingletonHolder {
        private static final BoxFactory INSTANCE = new BoxFactory();
    }


    public static final BoxFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }


    public AbstractBox getBox(String key) {
        return map.get(key);
    }
}