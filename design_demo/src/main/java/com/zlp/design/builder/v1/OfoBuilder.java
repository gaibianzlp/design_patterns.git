package com.zlp.design.builder.v1;

/**
 * ofo单车Builder类
 * @date: 2022/3/7 15:49
 */
public class OfoBuilder extends Builder {

    @Override
    public void buildFrame() {
        mBike.setFrame("碳纤维车架");
    }
    @Override
    public void buildSeat() {
        mBike.setSeat("橡胶车座");
    }
    @Override
    public Bike createBike() {
        return mBike;
    }
}