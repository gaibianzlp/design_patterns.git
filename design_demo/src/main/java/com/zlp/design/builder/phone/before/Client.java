package com.zlp.design.builder.phone.before;

/**
 * 测试类
 * @date: 2022/3/7 15:48
 */
public class Client {
    public static void main(String[] args) {
        //构建Phone对象
        Phone phone = new Phone("intel","三星屏幕","金士顿","华硕");
        System.out.println(phone);
    }
}