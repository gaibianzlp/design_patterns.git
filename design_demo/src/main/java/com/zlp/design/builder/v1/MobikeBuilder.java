package com.zlp.design.builder.v1;


/**
 * 摩拜单车Builder类
 * @date: 2022/3/7 15:49
 */
public class MobikeBuilder extends Builder {

    @Override
    public void buildFrame() {
        mBike.setFrame("铝合金车架");
    }

    @Override
    public void buildSeat() {
        mBike.setSeat("真皮车座");
    }

    @Override
    public Bike createBike() {
        return mBike;
    }
}