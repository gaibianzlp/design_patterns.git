package com.zlp.design.builder.v1;

/**
 * 抽象 builder 类
 * @date: 2022/3/7 15:49
 */
public abstract class Builder {

    protected Bike mBike = new Bike();

    public abstract void buildFrame();

    public abstract void buildSeat();

    public abstract Bike createBike();
}