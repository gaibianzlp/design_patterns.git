package com.zlp.design.builder.v1;

/**
 * 指挥者类
 * @date: 2022/3/7 15:49
 */
public class Director {

    private Builder mBuilder;

    public Director(Builder builder) {
        mBuilder = builder;
    }

    public Bike construct() {
        mBuilder.buildFrame();
        mBuilder.buildSeat();
        return mBuilder.createBike();
    }
}