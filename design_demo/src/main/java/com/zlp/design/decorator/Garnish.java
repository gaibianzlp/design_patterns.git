package com.zlp.design.decorator;

/**
 * 配料类
 * @date: 2022/3/4 13:39
 */
public abstract class Garnish extends FastFood {

    private FastFood fastFood;


    public Garnish(FastFood fastFood, float price, String desc) {
        super(price,desc);
        this.fastFood = fastFood;
    }

    public FastFood getFastFood() {
        return fastFood;
    }

}
