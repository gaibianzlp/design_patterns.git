package com.zlp.design.decorator;

/**
 * 炒饭
 * @date: 2022/3/4 13:39
 */
public class FriedRice extends FastFood {

    public FriedRice() {
        super(10, "炒饭");
    }

    public float cost() {
        return getPrice();
    }
}