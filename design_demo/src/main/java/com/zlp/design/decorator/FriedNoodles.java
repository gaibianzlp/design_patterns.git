package com.zlp.design.decorator;

/**
 * 炒面
 * @date: 2022/3/4 13:39
 */
public class FriedNoodles extends FastFood {

    public FriedNoodles() {
        super(12, "炒面");
    }

    public float cost() {
        return getPrice();
    }
}