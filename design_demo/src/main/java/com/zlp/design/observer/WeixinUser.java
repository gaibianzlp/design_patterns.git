package com.zlp.design.observer;

/**
 * 微信用户（具体观察者）
 * @date: 2022/3/11 15:59
 */
public class WeixinUser implements Observer {
    // 微信用户名
    private String name;

    public WeixinUser(String name) {
        this.name = name;
    }
    @Override
    public void update(String message) {
        System.out.println(name + "-" + message);
    }
}