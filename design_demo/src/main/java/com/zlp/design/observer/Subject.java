package com.zlp.design.observer;

/**
 * 抽象主题
 * @date: 2022/3/11 15:55
 */
public interface Subject {

    /**
     * 增加订阅者
     * @date: 2022/3/11 15:55
     */
    void attach(Observer observer);

    /**
     * 删除订阅者
     * @date: 2022/3/11 15:55
     */
    void detach(Observer observer);
    
    /**
     * 通知订阅者更新消息
     * @date: 2022/3/11 15:55
     */
    void notify(String message);
}