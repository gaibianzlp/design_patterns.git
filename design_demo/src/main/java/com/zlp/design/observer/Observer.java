package com.zlp.design.observer;
/**
 * 抽象观察者类
 * @date: 2022/3/11 15:55
 */
public interface Observer {

    void update(String message);
}