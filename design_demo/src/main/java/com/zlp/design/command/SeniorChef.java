package com.zlp.design.command;

public class SeniorChef {

    /**
     * 炒菜
     * @param num 份数
     * @param foodName 菜名
     * @date: 2022/3/10 15:56
     */
    public void makeFood(Integer num,String foodName){
        System.out.println("["+num+"]份"+foodName);
    }
}
