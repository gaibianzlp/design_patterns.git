package com.zlp.design.command;

import java.util.Set;

/**
 * 订单执行命令
 * @date: 2022/3/10 16:34
 */
public class OrderCommand implements Command {

    //持有接受者对象
    private SeniorChef receiver;
    private Order order;

    public OrderCommand(SeniorChef receiver, Order order){
        this.receiver = receiver;
        this.order = order;
    }

    /**
     * 执行命令
     * @date: 2022/3/10 16:44
     */
    public void execute()  {

        System.out.println(order.getTableNumber() + "桌的订单：");
        Set<String> keys = order.getFoodDic().keySet();
        for (String key : keys) {
            receiver.makeFood(order.getFoodDic().get(key),key);
        }

        try {
            Thread.sleep(100);//停顿一下 模拟做饭的过程
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(order.getTableNumber() + "桌的饭弄好了");
    }
}
