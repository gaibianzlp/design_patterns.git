package com.zlp.design.command;

import java.util.ArrayList;

/**
 * 服务员
 * @date: 2022/3/10 16:31
 */
public class Waiter {

    private ArrayList<Command> commands;//可以持有很多的命令对象

    public Waiter() {
        commands = new ArrayList();
    }

    public void setCommand(Command cmd){
        commands.add(cmd);
    }

    // 发出命令 喊 订单来了，厨师开始执行
    public void orderUp() {
        System.out.println("美女服务员：叮咚，大厨，新订单来了.......");
        for (int i = 0; i < commands.size(); i++) {
            Command cmd = commands.get(i);
            if (cmd != null) {
                System.out.println("=================================");
                cmd.execute();
            }
        }
    }
}
