package com.zlp.design.command;


public class Client {

    public static void main(String[] args) {


        Waiter waiter = new Waiter();
        Order order1 = new Order();
        order1.setTableNumber("1号");
        order1.getFoodDic().put("鱼香肉丝盖浇饭",1);
        order1.getFoodDic().put("小瓶可乐",1);

        Order order2 = new Order();
        order2.setTableNumber("2号");
        order2.getFoodDic().put("干锅鱼块",1);
        order2.getFoodDic().put("小瓶雪碧",1);

        //创建接收者
        SeniorChef receiver = new SeniorChef();

        // 将订单和接收者封装成命令对象
        Command command1 = new OrderCommand(receiver,order1);
        Command command2 = new OrderCommand(receiver,order2);
        waiter.setCommand(command1);
        waiter.setCommand(command2);
        //将订单带到柜台 并向厨师喊 订单来了
        waiter.orderUp();
    }
}
