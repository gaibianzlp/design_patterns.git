package com.zlp.design.command;


import java.util.HashMap;
import java.util.Map;

/**
 * 订单
 * @date: 2022/3/10 15:49
 */
public class Order {

    /**
     * 桌号
     */
    private String tableNumber;

    /**
     * 下单的菜品名称和数量
     */
    private Map<String, Integer> foodDic = new HashMap();


    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public Map<String, Integer> getFoodDic() {
        return foodDic;
    }

    public void setFoodDic(Map<String, Integer> foodDic) {
        this.foodDic = foodDic;
    }
}
