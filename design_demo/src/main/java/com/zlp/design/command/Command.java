package com.zlp.design.command;


/**
 * 执行命令
 * @date: 2022/3/10 16:22
 */
public interface Command {

    public void execute();
}
