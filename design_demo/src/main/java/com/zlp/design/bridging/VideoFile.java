package com.zlp.design.bridging;

/**
 * 视频文件
 * @date: 2022/3/9 10:12
 */
public interface VideoFile {


    /**
     * 视频解码器
     * @param fileName
     * @date: 2022/3/9 10:12
     */
    void decode(String fileName);
}