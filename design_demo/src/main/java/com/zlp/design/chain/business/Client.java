package com.zlp.design.chain.business;
import java.math.BigDecimal;
import java.util.Date;


public class Client {


    public static void main(String[] args) {

        ServiceChain serviceChain = new ServiceChain();
        OrderVO orderVO = new OrderVO();
        orderVO.setOrderId(1);
        orderVO.setOrderName("iphone13pro");
        orderVO.setOrderStatus(1);
        orderVO.setOrderAmount(new BigDecimal("5399"));
        orderVO.setOrderType(1);
        orderVO.setOrderDate(new Date());

        serviceChain.addService(new ParamService());
        serviceChain.addService(new CalculationService());
        serviceChain.addService(new ExecuteService());
        serviceChain.doService(orderVO);
    }
}