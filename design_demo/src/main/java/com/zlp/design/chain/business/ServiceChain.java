package com.zlp.design.chain.business;

import java.util.ArrayList;
import java.util.List;

public class ServiceChain {

    private List<Command> services = new ArrayList<>();
    private int index = 0;

    // 链式调用
    public ServiceChain addService(Command command) {
        this.services.add(command);
        return this;
    }

    public void doService(Object obj) {
        if (index == services.size()) {
            return;
        }
        Command command = services.get(index);
        index++;
        command.doExecute(obj, this);
    }
}