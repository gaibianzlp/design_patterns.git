package com.zlp.design.chain.business;

public interface Command {

    Object doExecute(Object obj, ServiceChain serviceChain);
}
