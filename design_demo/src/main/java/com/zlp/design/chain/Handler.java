package com.zlp.design.chain;

/**
 * 处理者抽象类
 *
 * @date: 2022/3/10 17:18
 */
public abstract class Handler {

    /**
     * 请假天数
     */
    protected static final int NUM_ONE = 1;
    protected static final int NUM_THREE = 3;
    protected static final int NUM_SEVEN = 7;

    //该领导处理的请假天数区间
    private int numStart;
    private int numEnd;

    //领导上面还有领导->(后继者)
    private Handler nextHandler;

    //设置请假天数范围 上不封顶
    public Handler(int numStart) {
        this.numStart = numStart;
    }

    //设置请假天数范围
    public Handler(int numStart, int numEnd) {
        this.numStart = numStart;
        this.numEnd = numEnd;
    }

    //设置上级领导->(后继者)
    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    /**
     * 各级领导处理请假条方法
     * @param leave
     * @date: 2022/3/10 17:21
     * @return: void
     */
    protected abstract void handleLeave(LeaveRequest leave);

    /**
     * 提交请假条
     * @param leave
     * @date: 2022/3/10 17:21
     * @return: void
     */
    public final void submit(LeaveRequest leave) {
        if (0 == this.numStart) {
            return;
        }

        //如果请假天数达到该领导者的处理要求
        if (leave.getNum() >= this.numStart) {
            this.handleLeave(leave);

            //如果还有上级 并且请假天数超过了当前领导的处理范围
            if (null != this.nextHandler && leave.getNum() > numEnd) {
                this.nextHandler.submit(leave);//继续提交
            } else {
                System.out.println("流程结束");
            }
        }
    }

}