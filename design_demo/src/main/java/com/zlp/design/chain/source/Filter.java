package com.zlp.design.chain.source;

public interface Filter {

 	void doFilter(Request req,Response res,FilterChain c);
 }