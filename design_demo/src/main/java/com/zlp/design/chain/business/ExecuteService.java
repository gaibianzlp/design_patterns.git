package com.zlp.design.chain.business;


/**
 * 参数校验服务
 * @date: 2022/3/10 18:05
 */
public class ExecuteService implements Command {


    @Override
    public Object doExecute(Object obj, ServiceChain serviceChain) {

        System.out.println("第三步：执行业务方法");
        // 执行下一个业务逻辑
        serviceChain.doService(obj);

        return null;

    }
}