package com.zlp.design.chain.business;

import java.math.BigDecimal;
import java.util.Date;

public class OrderVO {

    private Integer orderId;
    private String orderName;
    private Integer orderStatus;
    private BigDecimal orderAmount;
    private Integer orderType;
    private Date orderDate;

    public OrderVO() {
    }

    public OrderVO(Integer orderId, String orderName, Integer orderStatus, BigDecimal orderAmount, Integer orderType, Date orderDate) {
        this.orderId = orderId;
        this.orderName = orderName;
        this.orderStatus = orderStatus;
        this.orderAmount = orderAmount;
        this.orderType = orderType;
        this.orderDate = orderDate;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
}
