package com.zlp.design.chain.business;


import java.math.BigDecimal;

/**
 * 参数校验服务
 * @date: 2022/3/10 18:05
 */
public class ParamService implements Command {


    @Override
    public Object doExecute(Object obj, ServiceChain serviceChain) {

        OrderVO orderVO = null;
        System.out.println("第一步：校验参数");
        if (obj instanceof OrderVO){
            orderVO = (OrderVO) obj;
            orderVO.setOrderAmount(new BigDecimal("999999"));
        }
        // 执行下一个业务逻辑
        serviceChain.doService(orderVO);
        return orderVO;

    }
}