package com.zlp.design.facade;


/**
 * 智能音箱
 * @date: 2022/3/9 14:26
 * @return: void
 */
public class SmartAppliancesFacade {

    private Light light;
    private TV tv;
    private AirCondition airCondition;

    public SmartAppliancesFacade() {
        light = new Light();
        tv = new TV();
        airCondition = new AirCondition();
    }


    /**
     * 语言控制
     * @date: 2022/3/9 14:26
     * @return: void
     */
    public void say(String message) {
        if (message.contains("打开")) {
            on();
        } else if (message.contains("关闭")) {
            off();
        } else {
            System.out.println("我还听不懂你说的！！！");
        }
    }

    /**
     * 起床后一键开电器
     * @date: 2022/3/9 14:26
     * @return: void
     */
    private void on() {
        System.out.println("起床了");
        light.on();
        tv.on();
        airCondition.on();

    }


    /**
     * 睡觉一键关电器
     * @date: 2022/3/9 14:26
     * @return: void
     */
    private void off() {

        System.out.println("睡觉了");
        light.off();
        tv.off();
        airCondition.off();
    }

}