package com.zlp.design.state.aftre;

/**
 * 测试类
 * @date: 2022/3/11 14:18
 */
public class Client {

    public static void main(String[] args) {
        Context context = new Context();
        context.setLiftState(new ClosingState());
        context.open();
        context.close();
        context.run();
        context.stop();
    }
}