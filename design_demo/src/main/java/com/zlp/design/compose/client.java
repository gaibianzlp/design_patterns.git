package com.zlp.design.compose;

public class client {

    public static void main(String[] args) {

        MenuComponent component = new Menu("系统管理",1);

        MenuComponent menu1 = new Menu("菜单管理",2);
        MenuComponent menuItem1 = new Menu("页面访问",3);
        MenuComponent menuItem2 = new Menu("编辑菜单",3);
        MenuComponent menuItem3 = new Menu("删除菜单",3);
        MenuComponent menuItem4 = new Menu("添加菜单",3);
        menu1.add(menuItem1);
        menu1.add(menuItem2);
        menu1.add(menuItem3);
        menu1.add(menuItem4);

        MenuComponent menu2 = new Menu("权限管理",2);
        MenuComponent menuItem6 = new Menu("页面访问",3);
        MenuComponent menuItem7 = new Menu("提交保存",3);
        menu2.add(menuItem6);
        menu2.add(menuItem7);

        component.add(menu1);
        component.add(menu2);

        component.print();

//        System.exit(0);


    }
}
