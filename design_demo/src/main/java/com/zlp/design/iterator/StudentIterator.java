package com.zlp.design.iterator;

public interface StudentIterator {

    boolean hasNext();
    Student next();
}