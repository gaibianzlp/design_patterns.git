package com.zlp.design.iterator;

public class Client {

    public static void main(String[] args) {

        StudentAggregate studentAggregate = new StudentAggregateImpl();
        studentAggregate.addStudent(new Student("001","刘明1"));
        studentAggregate.addStudent(new Student("002","刘明2"));
        studentAggregate.addStudent(new Student("003","刘明3"));
        studentAggregate.addStudent(new Student("004","刘明4"));
        studentAggregate.addStudent(new Student("005","刘明5"));

        StudentIterator studentIterator = studentAggregate.getStudentIterator();
        // 遍历
        while (studentIterator.hasNext()) {
            Student student = studentIterator.next();
            System.out.println(student.toString());
        }


    }
}
