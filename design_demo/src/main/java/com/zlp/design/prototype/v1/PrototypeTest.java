package com.zlp.design.prototype.v1;

public class PrototypeTest {


    public static void main(String[] args) throws CloneNotSupportedException {
        Realizetype r1 = new Realizetype();
        r1.setIndex(156);
        r1.setName("给自己一个smile");
        Realizetype r2 = r1.clone();
        System.out.println("对象r1和r2是同一个对象？" + (r1 == r2));
        System.out.println("对象r1.index和r2.index是同一个对象？" + (r1.getIndex() == r2.getIndex()));
        System.out.println("对象r1.name和r2.name是同一个对象？" + (r1.getName() == r2.getName()));
    }
}