package com.zlp.design.prototype.v3;

import java.io.Serializable;

/**
 * Student 学生类
 * @date: 2022/3/22 16:44
 */
public class Student implements Serializable {

    private String name;
    private String address;
    public Student(String name, String address) {
        this.name = name;
        this.address = address;
    }
    public Student() {
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}