package com.zlp.design.demo.example.v2;

public class Driver {

    private String name;

    public Driver(String name) {
        this.name = name;
    }

    public void driver(Car car){

        System.out.println(name+"-驾驶"+car.move()+"汽车");
    }
}
