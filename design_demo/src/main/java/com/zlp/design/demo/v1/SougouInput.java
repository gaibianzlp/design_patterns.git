package com.zlp.design.demo.v1;

/**
 * 搜狗输入法
 * @date: 2022/3/4 13:37
 */
public class SougouInput {

    private AbstractSkin skin;

    public SougouInput(){

    }

    public SougouInput(AbstractSkin skin){
        this.skin = skin;
    }

    public void display(){
        skin.display();
    }


}
