package com.zlp.design.demo.v2.after;

/**
 * 测试方法
 * @date: 2022/3/4 13:49
 */
public class RectangleDemo {

    /**
     * 调整大小
     * @param rectangle
     * @date: 2022/3/4 13:52
     */
    public static void resizeRectangle(Rectangle rectangle) {
        while (rectangle.getWidth() <= rectangle.getLength()) {
            rectangle.setWidth(rectangle.getWidth() + 1);
        }
    }

    /**
     * 打印四边形形的长和宽
     * @param quadrilateral
     * @date: 2022/3/4 13:52
     */
    public static void printLengthAndWidth(Quadrilateral quadrilateral) {
        System.out.println(quadrilateral.getLength());
        System.out.println(quadrilateral.getWidth());
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setLength(20);
        rectangle.setWidth(10);
        resizeRectangle(rectangle);
        printLengthAndWidth(rectangle);

    }
}