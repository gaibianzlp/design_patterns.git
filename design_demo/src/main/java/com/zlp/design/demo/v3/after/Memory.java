package com.zlp.design.demo.v3.after;


/**
 * 内存条接口
 * @date: 2022/3/4 14:38
 */
public interface Memory {

    public void save();
}
