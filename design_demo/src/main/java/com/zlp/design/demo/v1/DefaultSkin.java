package com.zlp.design.demo.v1;

/**
 * 默认皮肤
 * @date: 2022/3/4 13:39
 */
public  class DefaultSkin extends AbstractSkin{

    @Override
    public void display() {

        System.out.println("默认皮肤设置");
    }
}
