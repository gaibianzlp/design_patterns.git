package com.zlp.design.demo.v4.after;

public class ItcastSafetyDoor implements AntiTheft,Fireproof {

    public void antiTheft() {

        System.out.println("防盗");

    }

    public void fireproof() {

        System.out.println("防火");

    }

}