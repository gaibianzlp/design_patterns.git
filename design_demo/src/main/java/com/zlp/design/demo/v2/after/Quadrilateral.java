package com.zlp.design.demo.v2.after;

public interface Quadrilateral {

    double getLength();

    double getWidth();
}
