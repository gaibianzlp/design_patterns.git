package com.zlp.design.demo.v5;

public class Test {

    public static void main(String[] args) {
        Agent agent = new Agent();
        agent.setCompany(new Company("人生无极限公司"));
        agent.setFans(new Fans("zouLiPing "));
        agent.setStar(new Star(" MayDay "));

        agent.meeting();
        System.out.println("==============================");
        agent.business();
    }
}
