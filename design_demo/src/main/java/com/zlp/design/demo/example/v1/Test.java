package com.zlp.design.demo.example.v1;

public class Test {

    public static void main(String[] args) {

        Counter counter = Counter.getInstance();
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                counter.add();
                System.out.println(counter.getOnline());
            }).start();
        }
    }
}
