package com.zlp.design.demo.v4.before;

public class HeiMaSafetyDoor implements SoftDoor {


    @Override
    public void antiTheft() {
        System.out.println("具有防盗功能...");
    }

    @Override
    public void fireproof() {
        System.out.println("具有防火功能...");
    }

    @Override
    public void waterproof() {
        System.out.println("具有防水功能...");
    }
}
