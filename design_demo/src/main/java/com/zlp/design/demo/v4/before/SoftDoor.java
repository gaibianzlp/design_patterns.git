package com.zlp.design.demo.v4.before;

public interface SoftDoor {


    /**
     * 防盗
     * @date: 2022/3/4 15:27
     * @return: void
     */
    void antiTheft ();

    /**
     * 防火
     * @date: 2022/3/4 15:27
     * @return: void
     */
    void fireproof ();

    /**
     * 防水
     * @date: 2022/3/4 15:27
     * @return: void
     */
    void waterproof  ();
}
