package com.zlp.design.demo.v5;

public class Star {

    private String name;

    public Star(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }
}