package com.zlp.design.demo.v2.after;

/**
 * 正方形
 * @date: 2022/3/4 13:49
 */
public class Square implements  Quadrilateral{

    private double side;

    public void setSide(double side) {
           this.side = side;
    }


    @Override
    public double getLength() {
        return side;
    }

    @Override
    public double getWidth() {
        return side;
    }
}