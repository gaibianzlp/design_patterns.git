package com.zlp.design.demo.v4.before;

public class SafetyDoorTest {

    public static void main(String[] args) {

        HeiMaSafetyDoor heiMaSafetyDoor = new HeiMaSafetyDoor();
        heiMaSafetyDoor.antiTheft();
        heiMaSafetyDoor.waterproof();
        heiMaSafetyDoor.fireproof();
    }
}
