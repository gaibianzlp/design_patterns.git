package com.zlp.design.demo.v3.after;

public interface HardDisk {

    public void save(String data);

    public String get();
}
