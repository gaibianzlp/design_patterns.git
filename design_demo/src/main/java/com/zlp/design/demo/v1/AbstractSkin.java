package com.zlp.design.demo.v1;


/**
 * 抽象类皮肤
 * @date: 2022/3/4 13:39
 */
public abstract class AbstractSkin {

    public abstract void display();
}
