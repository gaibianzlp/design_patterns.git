package com.zlp.design.demo.example.v1;

import java.util.concurrent.atomic.AtomicLong;

public class Counter {
   
    private Counter(){
        System.out.println("init...");
    }

    private static class CounterHolder{
        private static final Counter counter = new Counter();
    }


    public static final Counter getInstance(){
        return CounterHolder.counter;
    }
 
    private AtomicLong online = new AtomicLong();
 
    public long getOnline(){
        return online.get();
    }
 
    public long add(){
        return online.incrementAndGet();
    }
}    