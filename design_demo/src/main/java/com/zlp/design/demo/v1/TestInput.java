package com.zlp.design.demo.v1;

/**
 * 测试输入法配置
 * @date: 2022/3/4 13:39
 */
public class TestInput {

    public static void main(String[] args) {

        SougouInput sougouInput = new SougouInput(new DefaultSkin());
        sougouInput.display();
    }
}
