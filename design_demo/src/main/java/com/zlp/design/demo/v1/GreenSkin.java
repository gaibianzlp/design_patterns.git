package com.zlp.design.demo.v1;

/**
 * 绿色皮肤
 * @date: 2022/3/4 13:39
 */
public  class GreenSkin extends AbstractSkin{

    @Override
    public void display() {

        System.out.println("绿色皮肤设置");
    }
}
