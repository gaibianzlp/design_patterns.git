package com.zlp.design.mediator;

/**
 * 抽象同事类
 * @date: 2022/3/11 17:28
 */
public abstract class Person {

    protected String name;
    protected Mediator mediator;

    public Person(String name,Mediator mediator){
        this.name = name;
        this.mediator = mediator;
    }
}