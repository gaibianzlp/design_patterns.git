package com.zlp.design.mediator;

/**
 * 抽象中介者
 * @date: 2022/3/11 17:28
 */
public abstract class Mediator {

    /**
     * 申明一个联络方法
     * @date: 2022/3/11 17:28
     */
    public abstract void constact(String message,Person person);
}