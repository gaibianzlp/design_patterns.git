package com.zlp.design.strategy;

/**
 * 抽象策略
 * @date: 2022/3/10 10:13
 */
public interface Strategy {
    /**
     * 活动
     * @date: 2022/3/10 10:14
     */
    void show();
}