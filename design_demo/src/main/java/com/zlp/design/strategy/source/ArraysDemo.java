package com.zlp.design.strategy.source;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ArraysDemo {


    public static void main(String[] args) {

        List<Integer> list = java.util.Arrays.asList(1, 2, 3);

        list.sort(Comparator.comparingInt(Integer::intValue).reversed()); // 倒序
        print(list);
        System.out.println("==========================");

        list.sort(Comparator.comparingInt(Integer::intValue)); // 默认正序
        print(list);

        System.out.println("stream==========================");
        List<Integer> list2 = Arrays.asList(1, 2, 3);
        list2 = list2.stream().sorted(Comparator.comparingInt(Integer::intValue).reversed()).collect(Collectors.toList()); // 倒序

        print(list2);

        list2 = list2.stream().sorted(Comparator.comparingInt(Integer::intValue)).collect(Collectors.toList()); // 默认正序

        print(list2);

    }

    /**
     * 输出
     */
    private static <T> void print(List<T> list) {

        list.forEach(e -> System.out.print(e + ","));

        System.out.println();
    }

}