package com.zlp.jdkproxy;

import com.zlp.jdkproxy.Subject;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 22:17
 **/
public class RealSubject implements Subject {

    @Override
    public int sellBooks() {
        System.out.println("卖书");
        return 1 ;
    }

    @Override
    public String speak() {
        System.out.println("说话");
        return "张三";
    }
}
