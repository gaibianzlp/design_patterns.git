package com.zlp.jdkproxy;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 22:14
 **/
public interface Subject {

     int sellBooks();

     String speak();
}
