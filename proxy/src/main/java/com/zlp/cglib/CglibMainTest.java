package com.zlp.cglib;

/**
 * @description: //TODO
 * @author: LiPing.Zou
 * @create: 2020-05-09 22:41
 **/
public class CglibMainTest {

    public static void main(String[] args) {
        // 生成 Cglib 代理类
        Engineer engineerProxy = (Engineer) CglibProxy.getProxy(new Engineer());
        // 调用相关方法
        engineerProxy.eat();
    }
}
