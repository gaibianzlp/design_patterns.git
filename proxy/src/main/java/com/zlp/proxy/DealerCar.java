package com.zlp.proxy;

import com.zlp.service.BydCar;
import com.zlp.service.impl.TangCar;

/**
 * @description: 经销商
 * @author: LiPing.Zou
 * @create: 2020-05-09 20:18
 **/
public class DealerCar implements BydCar {

    private TangCar tangCar;



    @Override
    public void car() {

        if (tangCar == null){
            tangCar = new TangCar();
        }
        tangCar.car();
    }
}
