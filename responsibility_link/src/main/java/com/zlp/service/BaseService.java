package com.zlp.service;


import com.zlp.handler.ServiceContext;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/21 14:42
 * @description：${description}
 */
public interface BaseService {
    /**
     * 执行条件
     *
     * @param context
     * @return
     */
    boolean condition(ServiceContext context);

    /**
     * 执行业务逻辑
     *
     * @param context
     */
    void process(ServiceContext context);
}