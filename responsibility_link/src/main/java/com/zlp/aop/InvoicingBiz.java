package com.zlp.aop;

import com.zlp.handler.ProcessName;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/7 15:49
 * @description: 订单验证注解
 * 所有打上该注解的类都会被动态调用去验证下单逻辑
 */
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InvoicingBiz {
    /**
     * 验证类唯一键值
     *
     * @return
     */
    String value() default "";

    /**
     * 验证类执行顺序
     *
     * @return
     */
    int order() default 0;

    /**
     * 扩展字段暂时不用
     *
     * @return
     */
    String group() default "";

    ProcessName processName() default ProcessName.Null_Default;

}
