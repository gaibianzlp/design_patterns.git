package com.zlp.handler;

public enum OrderStatus {

    ACCEPT_ORDER_STATUS(1, "受订"),
    //ID为4
    PENDING_ORDER_STATUS(2 << 1, "待制"),
    //ID为8
    MAKING_ORDER_STATUS(8, "在制"),
    //ID为16
    IN_THE_WAREHOUSE_ORDER_STATUS(2 << 3, "在库"),
    //ID为32
    TO_BE_DELIVERED_ORDER_STATUS(2 << 4, "待发"),
    //ID为64
    LOADING_ORDER_STATUS(2 << 5, "装车中"),
    //ID为128
    ON_THE_WAY_ORDER_STATUS(2 << 6, "在途"),
    //ID为256
    THE_ARRIVAL_ORDER_STATUS(2 << 7, "到货"),
    //ID为512
    STORAGE_ORDER_STATUS(2 << 8, "入库"),
    //ID为1024
    PENDING_REVIEW_ORDER_STATUS(2 << 9, "待审"),
    //ID为2048
    RETURN_ORDER_STATUS(2 << 10, "退回"),
    //ID为4096
    INVALID_ORDER_STATUS(2 << 11, "失效"),
    //ID为8192
    OUT_WAREHOUSE_CLOSING_CASE_STATUS(2 << 12, "出库结案"),
    //ID为16384
    ENTER_WAREHOUSE_CLOSING_CASE_STATUS(2 << 13, "入库结案"),
    //ID为32768
    ORDER_BUYING_STATUS(2 << 14, "申购中"),
    ORDER_Error_STATUS(2 << 15, "订单异常"),
    //ID 131072
    AGENT_APPROVAL_STATUS(2 << 16,"代理商代审"),
    PRE_ORDER_STATUS(2 << 17,"预订状态"),
    WAIT_RETURNED(2 << 18,"")
    ;

    private Integer key;
    private String value;

    OrderStatus(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Object getEnumValue() {
        return Long.valueOf(key.longValue());
    }

    public static Object getEnum(Object key) {
        for (OrderStatus type : OrderStatus.values()) {

            if (type.key.equals(Integer.valueOf(String.valueOf(key)))) {
                return type;
            }
        }
        return null;

    }

}