package com.zlp.handler;

public enum  BizType {

     NULL("默认",1),
     Order_Sync("订单同步",2),
     Order_Update("订单更新",3)
    ;
    private String value;
    private Integer index;

    private BizType(String value, Integer index) {
        this.value = value;
        this.index = index;
    }

    public String getValue() {
        return this.value;
    }
}