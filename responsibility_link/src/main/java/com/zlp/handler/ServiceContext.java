package com.zlp.handler;

public class ServiceContext<T> {

    private ServiceRequest request=new ServiceRequest();
    private ServiceResponse response=new ServiceResponse();
    private ChangeStatusConfig rootConfig;

    /**
     * 业务种类
     */
    private BizType bizType;


    public BizType getBizType() {
        return bizType;
    }

    public void setBizType(BizType bizType) {
        this.bizType = bizType;
    }


    public ChangeStatusConfig getRootConfig() {
        return rootConfig;
    }

    public void setRootConfig(ChangeStatusConfig rootConfig) {
        this.rootConfig = rootConfig;
    }

    public ChangeStatusConfig getCurrentConfig() {
        return currentConfig;
    }

    public void setCurrentConfig(ChangeStatusConfig currentConfig) {
        this.currentConfig = currentConfig;
    }

    private ChangeStatusConfig currentConfig;
    public ServiceRequest getRequest() {
        return request;
    }

    public Boolean getBreakFlag() {
        return breakFlag;
    }

    public void setBreakFlag(Boolean breakFlag) {
        this.breakFlag = breakFlag;
    }

    /**
     * 该标识 将会停止所有后续操作
     */
    private Boolean breakFlag;

    public T getConditionConfig() {
        return conditionConfig;
    }

    public void setConditionConfig(T conditionConfig) {
        this.conditionConfig = conditionConfig;
    }

    private T conditionConfig;

    public ServiceResponse getResponse() {
        return response;
    }

    public <T>void createRequestBody(T body)
    {
        request.setBody(body);
    }


    public <T>void createResponseBody(T body)
    {
        response.setBody(body);
    }









}

