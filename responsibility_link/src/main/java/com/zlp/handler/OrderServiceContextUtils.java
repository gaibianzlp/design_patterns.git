package com.zlp.handler;

import com.zlp.dto.OrderDTO;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/11 11:59
 * @description： 订单上下文关联类辅助方法
 */
public class OrderServiceContextUtils {

    private static final String LOCK_KEY = "lock_order";

    private static final String TARGET_CREATOR_KEY = "target_creator";

    private static final String MSG_TOKEN_KEY = "msg_id";

    private static final String CREDIT_OCCUPY_KEY = "credit_occupy";

    private static final String STOCK_OCCUPY_GEN_KEY = "_stock_occupy_";

    private static final String TARGET_ORDER_KEY = "_target_order";
    private static final String SOURCE_ORDER_KEY = "_source_order";
    private static final String ORDER_ALL_MERCHANDISE_INFO = "_order_all_merchandise_info";

    private static final String ORDER_UPDATE_ORDER_INFO = "_order_update_original_info_";
    private static final String ORDER_UPDATE_ORDER_POLICY_INFO = "_order_update_original_policy_info_";
    private static final String ORDER_ORIGINAL_CUTBILL_INFO = "_order_original_cut_bill_info_";

    private static final String TARGET_MERCHANDISE_DATA = "target_merchandise_data";

    public static final String TECHNICAL_PRODUCTION = "technical_production";

    public static final String URGENT_NEED_FOR_PROCUREMENT = "urgent_need_for_procurement";

    public static final String PRODUCTION_PURCHASE_DISTRIBUTION = "production_purchase_distribution";
    /**
     * 超链供应商集合
     */
    public static final String HYPERCHAIN_SUPPLIER_COLLECTION = "hyperchain_supplier_collection";
    /**
     * 超链商品集合
     */
    public static final String HYPERCHAIN_MERCHANDISE_COLLECTION = "hyperchain_merchandise_collection";
    /**
     * 超链供应商分配
     */
    public static final String HYPERCHAIN_SUPPLIER_ASSIGNMENT = "hyperchain_supplier_assignment";

    public static OrderDTO getOrder(ServiceContext context) {
        return (OrderDTO) context.getRequest().getBody();
    }

    public static <T> T getResponseBody(ServiceContext context) {
        return (T) context.getResponse().getBody();
    }

    public static ServiceConditionConfig getServiceConditionConfig(ServiceContext serviceContext) {
        return (ServiceConditionConfig) serviceContext.getConditionConfig();
    }

    public static void setServiceConditionConfig(ServiceContext serviceContext, ServiceConditionConfig serviceConditionConfig) {
        serviceContext.setConditionConfig(serviceConditionConfig);
    }

//    /**
//     * 获取订单初始化信息
//     *
//     * @param context
//     * @return
//     */
//    public static OrderInfoInitializationDTO getOrderInfoInitializationInfo(ServiceContext context) {
//        return (OrderInfoInitializationDTO) context.getResponse().getBody();
//    }
//
//    public static PaymentTermsDTO getPayment(ServiceContext context) {
//        //付款条件
//        PaymentTermsDTO paymentTerms = getOrder(context).getPaymentTerms();
//        return paymentTerms;
//    }
//
//    /**
//     * 获取前端传入的商品信息
//     *
//     * @param context
//     * @return
//     */
//    public static OrderMerchandiseListDTO getOrderMerchandiseList(ServiceContext context) {
//        OrderMerchandiseListDTO orderMerchandiseListDTO = new OrderMerchandiseListDTO();
//        List<OrderMerchandiseDTO> merchandiseDTOList = getOrder(context).getOrderMerchandiseDTO();
//        orderMerchandiseListDTO.setMerchandiseDTOList(merchandiseDTOList);
//        orderMerchandiseListDTO.setOrderCategory(getOrder(context).getOrderCategory());
//        orderMerchandiseListDTO.setOrderType(getOrder(context).getOrderType());
//        orderMerchandiseListDTO.setCustomerId(getOrder(context).getCustomerId());
//        orderMerchandiseListDTO.setPayTerms(getOrder(context).getPaymentTerms().getPayTerm());
//        orderMerchandiseListDTO.setUseCutBillFlag(getOrder(context).getUseCutBillFlag());
//        return orderMerchandiseListDTO;
//    }
//
//    /**
//     * 获取前端传入的商品信息
//     *
//     * @param context
//     * @return
//     */
//    public static List<OrderMerchandiseDTO> getOrderMerchandise(ServiceContext context) {
//        List<OrderMerchandiseDTO> merchandiseDTOList = getOrder(context).getOrderMerchandiseDTO();
//        return merchandiseDTOList;
//    }
//
//
//    /**
//     * 获取付款方式
//     *
//     * @param context
//     * @return
//     */
//    public static List<PayKindDTO> getPayKindList(ServiceContext context) {
//        List<PayKindDTO> payKindlist = getOrderInfoInitializationInfo(context).getPayKindList();
//        return payKindlist;
//    }
//
//    /**
//     * 获取商品政策
//     *
//     * @param context
//     * @return
//     */
//    public static List<AuthPolicyNameListDTO> getMerchandisePolicy(ServiceContext context) {
//        List<AuthPolicyNameListDTO> authPolicyNameList = getOrderInfoInitializationInfo(context).getAuthPolicyNameList();
//        return authPolicyNameList;
//    }
//
//    /**
//     * 出库结案实体
//     *
//     * @param context
//     * @return
//     */
//    public static StockOutChangeBO getStockOutChangeBO(ServiceContext context) {
//        StockOutChangeBO stockOutChangeBO = (StockOutChangeBO) context.getRequest().getBody();
//        return stockOutChangeBO;
//    }

    public static void addHeader(String key, Object value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(key, value);

    }

    public static <T> void addTargetCreator(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(TARGET_CREATOR_KEY, value);
    }

    public static <T> T getTargetCreator(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(TARGET_CREATOR_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addMsgToken(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(MSG_TOKEN_KEY, value);
    }


    public static <T> T getMsgToken(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(MSG_TOKEN_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addOrderAllMerchandiseMap(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(ORDER_ALL_MERCHANDISE_INFO, value);
    }


    public static <T> T getOrderAllMerchandiseMap(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(ORDER_ALL_MERCHANDISE_INFO);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addUpdateOrderOriginalInfo(T value, ServiceContext context, String key) {
        context.getRequest().setHeaderValueByKey(ORDER_UPDATE_ORDER_INFO + key, value);
    }

    public static <T> T getUpdateOrderOriginalInfo(ServiceContext context, String key) {
        Object v = context.getRequest().getHeaderValueByKey(ORDER_UPDATE_ORDER_INFO + key);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addUpdateOrderPolicyOriginalInfo(T value, ServiceContext context, String key) {
        context.getRequest().setHeaderValueByKey(ORDER_UPDATE_ORDER_POLICY_INFO + key, value);
    }

    public static <T> T getUpdateOrderPolicyOriginalInfo(ServiceContext context, String key) {
        Object v = context.getRequest().getHeaderValueByKey(ORDER_UPDATE_ORDER_POLICY_INFO + key);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addOrderOriginalCutBillInfo(T value, ServiceContext context, String key) {
        context.getRequest().setHeaderValueByKey(ORDER_ORIGINAL_CUTBILL_INFO + key, value);
    }

    public static <T> T getOrderOriginalCutBillInfo(ServiceContext context, String key) {
        Object v = context.getRequest().getHeaderValueByKey(ORDER_ORIGINAL_CUTBILL_INFO + key);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }


    public static <T> void addCreditOccupy(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(CREDIT_OCCUPY_KEY, value);
    }

    public static <T> T getCreditOccupy(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(CREDIT_OCCUPY_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addTargetOrder(String orderId, T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(orderId + TARGET_ORDER_KEY, value);
    }

    public static <T> T getTargetOrder(ServiceContext context, String orderId) {
        Object v = context.getRequest().getHeaderValueByKey(orderId + TARGET_ORDER_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addTargetOrderDTO(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(TARGET_ORDER_KEY, value);
    }

    public static <T> T getTargetOrderDTO(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(TARGET_ORDER_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }


    public static <T> void addSourceOrder(String orderId, T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(orderId + SOURCE_ORDER_KEY, value);
    }

    public static <T> T getSourceOrder(ServiceContext context, String orderId) {
        Object v = context.getRequest().getHeaderValueByKey(orderId + SOURCE_ORDER_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addTargetMerchandiseData(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(TARGET_MERCHANDISE_DATA, value);
    }

    public static <T> T getTargetMerchandiseData(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(TARGET_MERCHANDISE_DATA);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addTargetMerchandiseData(String key, T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(key + TARGET_MERCHANDISE_DATA, value);
    }

    public static <T> T getTargetMerchandiseData(String key, ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(key + TARGET_MERCHANDISE_DATA);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }


    public static void addLock(Object value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(LOCK_KEY, value);

    }

    public static void removeLock(ServiceContext context) {
        context.getRequest().removeHeaderKey(LOCK_KEY);
    }

    public static void removeLock(ServiceContext context, String key) {
        String lockValue = getLockValue(context);

        if (lockValue != null) {
            lockValue = lockValue.replaceAll(key + ",", "");
            lockValue = lockValue.replaceAll("," + key, "");
            lockValue = lockValue.replaceAll(key, "");

            addLock(lockValue, context);
        }

    }


    public static String getLockValue(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(LOCK_KEY);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return v.toString();
        }
    }

//    public static List<OrderMerchandiseDTO> getOrderAllMerchandise(ServiceContext context, boolean needTying, boolean needBuyGift) {
//        OrderDTO orderDTO = getOrder(context);
//
//        return getOrderAllMerchandiseByOrder(orderDTO, needTying, needBuyGift);
//    }
//
//    public static List<OrderMerchandiseDTO> getOrderAllMerchandiseByOrder(OrderDTO orderDTO, boolean needTying, boolean needBuyGift) {
//        List<OrderMerchandiseDTO> merchandiseList = orderDTO.getOrderMerchandiseDTO();
//        List<OrderMerchandiseDTO> tempList = new ArrayList<>();
//        if (merchandiseList != null && !merchandiseList.isEmpty()) {
//            tempList.addAll(merchandiseList);
//            merchandiseList.forEach(orderMerchandise -> {
//                //获取子商品
//                List<OrderMerchandiseDTO> optionOrderMerchandise = orderMerchandise.getOrderMerchandiseDTOList();
//
//                if (optionOrderMerchandise != null && !optionOrderMerchandise.isEmpty()) {
//                    if (needTying) {
//                        List<OrderMerchandiseDTO> typingOrderMerchandise = optionOrderMerchandise.stream().filter(p -> p.getPolicyType().equals(PolicyType.Tying) &&
//                                p.getKind().equals(GiftPolicyMerchandiseKind.Gift)).collect(Collectors.toList());
//
//                        if (typingOrderMerchandise != null && typingOrderMerchandise.size() > 0) {
//                            tempList.addAll(typingOrderMerchandise);
//
//                        }
//                    }
//                    if (needBuyGift) {
//                        List<OrderMerchandiseDTO> giftOrderMerchandise = optionOrderMerchandise.stream().filter(p -> p.getPolicyType().equals(PolicyType.Buy_Gifts) &&
//                                p.getKind().equals(GiftPolicyMerchandiseKind.Gift)).collect(Collectors.toList());
//
//                        if (giftOrderMerchandise != null && giftOrderMerchandise.size() > 0) {
//                            tempList.addAll(giftOrderMerchandise);
//
//                        }
//
//                    }
//
//                }
//            });
//        }
//
//        return tempList;
//    }
//
//    public static void convertMerchandiseAuthPolicyMapToObject(List<AuthPolicyNameListDTO> authPolicyNameListDTOList) {
//        if (authPolicyNameListDTOList != null && !authPolicyNameListDTOList.isEmpty()) {
//            authPolicyNameListDTOList.forEach(m -> {
//
//                Map<PolicyType, List> hidePolicyList = m.getMerchandise().getHidePolicyList();
//                if (hidePolicyList != null && hidePolicyList.size() > 0) {
//                    Map<PolicyType, List> result = convertMerchandisePolicyMapToObj(hidePolicyList);
//                    m.getMerchandise().setHidePolicyList(result);
//                }
//
//                Map<PolicyType, List> mustPolicyList = m.getMerchandise().getMustPolicyList();
//                if (mustPolicyList != null && mustPolicyList.size() > 0) {
//                    Map<PolicyType, List> result = convertMerchandisePolicyMapToObj(mustPolicyList);
//                    m.getMerchandise().setMustPolicyList(result);
//                }
//
//                Map<PolicyType, List> optionPolicyList = m.getMerchandise().getOptionPolicyList();
//                if (optionPolicyList != null && optionPolicyList.size() > 0) {
//                    Map<PolicyType, List> result = convertMerchandisePolicyMapToObj(optionPolicyList);
//                    m.getMerchandise().setOptionPolicyList(result);
//                }
//
//                Map<PolicyType, List> otherPolicyList = m.getMerchandise().getOtherPolicyList();
//                if (otherPolicyList != null && otherPolicyList.size() > 0) {
//                    Map<PolicyType, List> result = convertMerchandisePolicyMapToObj(otherPolicyList);
//                    m.getMerchandise().setOtherPolicyList(result);
//                }
//
//
//            });
//        }
//
//
//    }
//
//    private static Map<PolicyType, List> convertMerchandisePolicyMapToObj(Map<PolicyType, List> policyList) {
//
//        Map<PolicyType, List> policyTypeListMap = new HashMap<>();
//
//        policyList.forEach((k, v) -> {
//
//
//            Map<PolicyType, List> currentPolicyTypeListMap = new HashMap<>();
//
//            Class<?> type = PolicyParsingEntityAutoLoad.map.get(k);
//
//            List<Object> policyObjectList = new ArrayList<>();
//            v.forEach(m -> {
//                try {
//                    String jsonPerson = PolicyParsingEntityAutoLoad.readMapper.writeValueAsString(m);
//                    Object policyObject = (Object) PolicyParsingEntityAutoLoad.writeMapper.readValue(jsonPerson, type);
//                    policyObjectList.add(policyObject);
//                } catch (Exception ex) {
//                    throw new ServiceException(Code.Code_Map_To_Object_Error, ServiceException.Level.ERROR, ex);
//                }
//            });
//
//            currentPolicyTypeListMap.put(k, policyObjectList);
//            MapUtil.merge2ResultMap(policyTypeListMap, currentPolicyTypeListMap);
//
//        });
//
//        return policyTypeListMap;
//    }
//
//
//    public static String genOccupyStockKey(String merchandiseCode, int index) {
//        return merchandiseCode + STOCK_OCCUPY_GEN_KEY + index;
//    }
//
//    public static String genOccupyStockKey(OrderMerchandiseDTO orderMerchandise) {
//        return genOccupyStockKey(orderMerchandise.getMaterialCode(), orderMerchandise.getItemId().intValue());
//    }
//
//
//    /**
//     * 通过订单信息找到采购单号和销售单号
//     *
//     * @param orderDTO
//     * @return
//     */
//    public static SupplyDemandBO getSupplyDemandOrderInfo(OrderDTO orderDTO) {
//        SupplyDemandBO supplyDemandBO = new SupplyDemandBO();
//        if (orderDTO.getOrderType().equals(OrderType.Sale)) {
//            supplyDemandBO.setSaleOrder(orderDTO.getOrderNo());
//            supplyDemandBO.setSaleOrderId(orderDTO.getOrderId());
//
//            if (orderDTO.getSourceOrderNo() != null && !"".equals(orderDTO.getSourceOrderNo().trim())) {
//                supplyDemandBO.setPurchaseOrder(orderDTO.getSourceOrderNo());
//                supplyDemandBO.setPurchanseOrderId(orderDTO.getSourceOrderId());
//            } else {
//
//                supplyDemandBO.setPurchaseOrder(orderDTO.getTargetOrderNo() == null ? "" : orderDTO.getTargetOrderNo().trim());
//
//                supplyDemandBO.setPurchanseOrderId(orderDTO.getTargetOrderId());
//
//            }
//
//
//        } else if (orderDTO.getOrderType().equals(OrderType.Purchase)) {
//            supplyDemandBO.setPurchaseOrder(orderDTO.getOrderNo());
//            supplyDemandBO.setPurchanseOrderId(orderDTO.getOrderId());
//            if (orderDTO.getSourceOrderNo() != null && !"".equals(orderDTO.getSourceOrderNo().trim())) {
//                supplyDemandBO.setSaleOrder(orderDTO.getSourceOrderNo());
//                supplyDemandBO.setSaleOrderId(orderDTO.getSourceOrderId());
//            } else {
//                supplyDemandBO.setSaleOrder(orderDTO.getTargetOrderNo() == null ? "" : orderDTO.getTargetOrderNo().trim());
//
//                supplyDemandBO.setSaleOrderId(orderDTO.getTargetOrderId());
//            }
//
//        }
//
//        return supplyDemandBO;
//    }
//
//    //查询订单扭转过程中 用到的 key
//    public static String getPurchaseAndSaleOrderNo(OrderDTO orderDTO) {
//        SupplyDemandBO supplyDemandBO = getSupplyDemandOrderInfo(orderDTO);
//        String purchaseNo = supplyDemandBO.getPurchaseOrder();
//        String saleNo = supplyDemandBO.getSaleOrder();
//        return purchaseNo + ";" + saleNo;
//
//    }
//
//
//    public static List<OrderMerchandiseDTO> getAllValidStatusConfig(List<OrderMerchandiseDTO> allCurrentStatusMerchandiseList,
//                                                                    ChangeStatusConfig changeToStatus, Long currentItemId, ChangeStatusConfig rootStatus) {
//        List<OrderMerchandiseDTO> changeStatusMerchandiseList = new ArrayList<>();
//        ChangeStatusConfig currentRootConfig = rootStatus;
//        while (currentRootConfig != null) {
//
//            List<OrderMerchandiseDTO> rootMerchandiseList = getMerchandiseListByItemConfig(allCurrentStatusMerchandiseList,
//                    currentItemId, currentRootConfig);
//
//            if (!rootMerchandiseList.isEmpty()) {
//                if (rootMerchandiseList.size() > 1) {
//                    throw new ServiceException(Code.Code_Merchandise_Status_Error, ServiceException.Level.ERROR);
//                }
//                //如果根商品不为空表示已经找到根商品
//
//                changeStatusMerchandiseList.addAll(rootMerchandiseList);
//            }
//
//            //如果当前状态已经是需要扭转的状态
//            if (currentRootConfig.getCurrentStatus()
//                    .equals(changeToStatus.getCurrentStatus())) {
//                break;
//            }
//            //change to next status
//            currentRootConfig = currentRootConfig.getNextStatus();
//        }
//        return changeStatusMerchandiseList;
//    }
//
//    public static OrderMerchandiseDTO getRootMerchandise(List<OrderMerchandiseDTO> allCurrentStatusMerchandiseList,
//                                                         Long currentItemId, ChangeStatusConfig rootStatus) {
//        OrderMerchandiseDTO rootMerchandise = null;
//        ChangeStatusConfig currentRootConfig = rootStatus;
//        while (currentRootConfig != null) {
//
//            List<OrderMerchandiseDTO> rootMerchandiseList = getMerchandiseListByItemConfig(allCurrentStatusMerchandiseList,
//                    currentItemId, currentRootConfig);
//
//            if (!rootMerchandiseList.isEmpty()) {
//                if (rootMerchandiseList.size() > 1) {
//                    throw new ServiceException(Code.Code_Merchandise_Status_Error, ServiceException.Level.ERROR);
//                }
//                //如果根商品不为空表示已经找到根商品
//
//                rootMerchandise = rootMerchandiseList.get(0);
//                break;
//            }
//
//            //change to next status
//            currentRootConfig = currentRootConfig.getNextStatus();
//        }
//        return rootMerchandise;
//    }
//
//    public static List<OrderMerchandiseDTO> getMerchandiseListByItemConfig(List<OrderMerchandiseDTO> allCurrentStatusMerchandiseList
//            , Long currentItemId, ChangeStatusConfig currentRootConfig) {
//        return allCurrentStatusMerchandiseList.stream().filter(f ->
//                f.getItemId().equals(currentItemId) && f.getOrderStatus().getKey()
//                        == currentRootConfig.getCurrentStatus().getKey()
//        ).collect(Collectors.toList());
//    }
//
//    public static BigDecimal addMoney(BigDecimal a, BigDecimal b) {
//
//        if (a == null) {
//            a = BigDecimal.valueOf(0);
//        }
//
//        if (b == null) {
//            b = BigDecimal.valueOf(0);
//        }
//
//        return a.add(b);
//    }
//
//    public static BigDecimal sumTotalBusNum(List<OrderMerchandiseDTO> allCurrentStatusMerchandiseList, boolean exceptCurrent, ChangeStatusConfig currentStatus) {
//        //总扭转业务量
//        BigDecimal totalBusQuantity = BigDecimal.ZERO;
//        if (allCurrentStatusMerchandiseList != null && !allCurrentStatusMerchandiseList.isEmpty()) {
//
//            for (int i = 0; i < allCurrentStatusMerchandiseList.size(); i++) {
//                OrderMerchandiseDTO tempMerchandise = allCurrentStatusMerchandiseList.get(i);
//
//                if (exceptCurrent && currentStatus.getCurrentStatus().equals(tempMerchandise.getOrderStatus())) {
//                    continue;
//                }
//                OrderMerchandiseDTO p = allCurrentStatusMerchandiseList.get(i);
//                totalBusQuantity = totalBusQuantity.add(p.getBusinessQuantity());
//            }
//
//        }
//        return totalBusQuantity;
//    }
//
//    public static List<OrderStatusChangeBodyDTO> changeMakingNoticeToOrderChange(MqNoticeBody<OrderMakingNoticeBO> orderMakingNoticeBO) {
//
//        List<OrderStatusChangeBodyDTO> orderStatusChangeBodyDTOList = new ArrayList<>();
//        if (orderMakingNoticeBO != null && !orderMakingNoticeBO.getItemList().isEmpty()) {
//            orderMakingNoticeBO.getItemList().stream().collect(Collectors.groupingBy(OrderMakingNoticeBO::getOrderSn))
//                    .forEach((k, v) -> {
//                        OrderStatusChangeBodyDTO orderStatusChangeRequestDTO = new OrderStatusChangeBodyDTO();
//                        orderStatusChangeBodyDTOList.add(orderStatusChangeRequestDTO);
//
//                        orderStatusChangeRequestDTO.setOrderId(v.get(0).getOrderId());
//                        orderStatusChangeRequestDTO.setOrderNo(v.get(0).getOrderSn());
//                        orderStatusChangeRequestDTO.setBigUnitFlag(false);
//                        orderStatusChangeRequestDTO.setOrderStatusEnum(v.get(0).getStatusEnum());
//                        List<OrderStatusChangeItemDTO> itemList = new ArrayList<>();
//
//                        orderStatusChangeRequestDTO.setItemList(itemList);
//                        v.stream().collect(Collectors.groupingBy(OrderMakingNoticeBO::getOrderItemId, Collectors.summingDouble(OrderMakingNoticeBO::getInSystemQuantity)))
//                                .forEach((p, m) -> {
//                                    OrderStatusChangeItemDTO orderStatusChangeItemDTO = new OrderStatusChangeItemDTO();
//                                    orderStatusChangeItemDTO.setApplyNum(m);
//                                    orderStatusChangeItemDTO.setItemId(p);
//                                    itemList.add(orderStatusChangeItemDTO);
//                                });
//
//
//                    });
//
//        }
//
//        return orderStatusChangeBodyDTOList;
//
//    }


    public static <T> void addHyperchainSupplier(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(HYPERCHAIN_SUPPLIER_COLLECTION, value);
    }

    public static <T> T getHyperchainSupplier(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(HYPERCHAIN_SUPPLIER_COLLECTION);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addHyperchainMerchandise(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(HYPERCHAIN_MERCHANDISE_COLLECTION, value);
    }

    public static <T> T getHyperchainMerchandise(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(HYPERCHAIN_MERCHANDISE_COLLECTION);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

    public static <T> void addHyperchainSupplierAssignment(T value, ServiceContext context) {
        context.getRequest().setHeaderValueByKey(HYPERCHAIN_SUPPLIER_ASSIGNMENT, value);
    }

    public static <T> T getHyperchainSupplierAssignment(ServiceContext context) {
        Object v = context.getRequest().getHeaderValueByKey(HYPERCHAIN_SUPPLIER_ASSIGNMENT);
        boolean isNull = (v == null);
        if (isNull) {
            return null;
        } else {
            return (T) v;
        }
    }

}