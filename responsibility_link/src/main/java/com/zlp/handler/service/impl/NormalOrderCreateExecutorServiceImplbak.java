//package com.zlp.handler.service.impl;
//
//import com.zlp.common.AbstractExecutorService;
//import com.zlp.dto.OrderResponseDTO;
//import com.zlp.handler.*;
//import com.zlp.handler.service.NormalOrderCreateExecutorService;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Propagation;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.ArrayList;
//
//
///**
// * @author ：yangxingwen
// * @date ：Created in 2019/3/27 21:01
// * @description：常规下单
// */
//@Service
//
//public class NormalOrderCreateExecutorServiceImplbak extends AbstractExecutorService implements NormalOrderCreateExecutorService {
//
//
//    @Override
//    public void start(ServiceContext serviceContext) {
//
//        //添加执行业务类
//        ServiceConditionConfig serviceConditionConfig = new ServiceConditionConfig();
//        serviceConditionConfig.setRunAll(false);
//        serviceContext.setConditionConfig(serviceConditionConfig);
//        serviceConditionConfig.setProcessNameList(new ArrayList<>());
//        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Approval_Create_User_Verify);
//        serviceContext.setConditionConfig(serviceConditionConfig);
//        super.start(serviceContext);
//
//    }
//
//    @Override
//    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = 36000, rollbackFor = Exception.class)
//    public void run(ServiceContext serviceContext) {
//
//        ///根据order 顺序进行执行代码
//
//        super.run(serviceContext);
//    }
//
//    @Override
//    public void stop(ServiceContext serviceContext) {
//
//        OrderDTO order = OrderServiceContextUtils.getOrder(serviceContext);
//
//        OrderResponseDTO orderResponseDTO = new OrderResponseDTO();
//        orderResponseDTO.setOrderId(order.getOrderId());
//        orderResponseDTO.setOrderNo(order.getOrderNo());
//        serviceContext.createResponseBody(orderResponseDTO);
//    }
//
//
////    @Override
////    public void start(ServiceContext serviceContext) {
////        //添加执行业务类
////        ServiceConditionConfig serviceConditionConfig = new ServiceConditionConfig();
////        serviceConditionConfig.setRunAll(false);
////        serviceContext.setConditionConfig(serviceConditionConfig);
////        serviceConditionConfig.setProcessNameList(new ArrayList<>());
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Approval_Create_User_Verify);
////        //serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Merchandise_Data_FixedAssets_Car_Order);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Order_Data);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.INITIALIZATION_ORDER_CATEGORY);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_PayKind_Data);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Order_Status);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Merchandise_Auth_Pull_Data);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Merchandise_Non_Auth_Pull_Data);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Merchandise_Data);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Sync_Order_Data);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Merchandise_DealPrice);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.INITIALIZATION_WAREHOUSE_DATA);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_Merchandise_FacePrice);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_Merchandise_Info);
////
////        //如果是更新忽略当前步子
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Create_Order_NO);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_BasePolicy);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_Policy_Verify);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Calculation_Order_BasePrice);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Calculation_Order_Policy);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Merchandise_Product_Data);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.INITIALIZATION_WAREHOUSE_CHILD_DATA);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Super_Chain_Object_Judgment_Service_Impl);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Superchain_Merchandise_Init_Service);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Locker_Order_Open);
////        //如果是更新需要查询割账金额和原始割账记录
////        serviceConditionConfig.getProcessNameList().add(ProcessName.WAREHOUSE_CALCULATION);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Calculation_Order_Detail_Total_Calculation);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_Merchandise_DealPrice);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Calculation_Order_CutBill);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Non_Money_Calculation_Order_Policy);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Calculation_Order_Tax);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Calculation_Order_Total_Calculation);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_Payment_Info);
////
////        //下单不关心商品是否有科目，入库的时候去验证是否存在科目
//////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Order_Merchandise_Own_Subject_Info);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Amount_Purchase_Verify);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Amount_Sale_Verify);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Amount_Occupy_Calculation);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Verify_Total_Tax_And_Deal_Total_Price);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Insert_OrderInfo_Into_DB);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Initialization_Insert_Order_Merchandise_Info_DB);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Stock_Occupy_Calculation);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Insert_Order_Merchandise_Info_DB);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Amount_Occupy_Create);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.CutBill_Occupy_Calculation);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Excess_Service_Impl);
////
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Locker_Order_Close);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Sync_Send_MQ_Msg);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Approval_Data_Send_MQ_Msg);
////
////        //受订向生产发信息 和占库存执行条件一致
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Accept_Change_To_Manufacture_Apply);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Accept_Manufacture_Apply);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Excess_Message_Service_Impl);
////
////        //申请生产发送消息
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Application_Production_Initialization_Service_Impl);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Apply_For_Production_Send_Mq_Service_Impl);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Production_Purchase_Distribution_Service_Impl);
////        serviceConditionConfig.getProcessNameList().add(ProcessName.Order_Urgent_Need_For_Procurement_Service_Impl);
////        serviceContext.setConditionConfig(serviceConditionConfig);
////        super.start(serviceContext);
////    }
////
////    @Override
////    @Transactional(propagation = Propagation.REQUIRES_NEW, timeout = 36000, rollbackFor = Exception.class)
////    public void run(ServiceContext serviceContext) {
////
////        ///根据order 顺序进行执行代码
////
////        super.run(serviceContext);
////    }
////
////    @Override
////    public void stop(ServiceContext serviceContext) {
////
////        OrderDTO order = OrderServiceContextUtils.getOrder(serviceContext);
////
////        OrderResponseDTO orderResponseDTO = new OrderResponseDTO();
////        orderResponseDTO.setOrderId(order.getOrderId());
////        orderResponseDTO.setOrderNo(order.getOrderNo());
////        List<OrderMerchandiseResponseDTO> orderMerchandiseResponseDTOList = new ArrayList<>();
////        for (OrderMerchandiseDTO merchandise : order.getOrderMerchandiseDTO()) {
////            OrderMerchandiseResponseDTO orderMerchandiseResponseDTO = new OrderMerchandiseResponseDTO();
////            orderMerchandiseResponseDTO.setId(merchandise.getId());
////            orderMerchandiseResponseDTO.setItemId(merchandise.getItemId());
////            orderMerchandiseResponseDTO.setMaterialCode(merchandise.getMaterialCode());
////            orderMerchandiseResponseDTO.setMaterialId(merchandise.getMaterialId());
////            orderMerchandiseResponseDTO.setPurchaseOrderId(merchandise.getPurchaseOrderId());
////            orderMerchandiseResponseDTO.setSaleOrderId(merchandise.getSaleOrderId());
////            orderMerchandiseResponseDTOList.add(orderMerchandiseResponseDTO);
////        }
////        orderResponseDTO.setOrderMerchandiseResponse(orderMerchandiseResponseDTOList);
////        serviceContext.createResponseBody(orderResponseDTO);
////    }
//}
