package com.zlp.handler.service;


import com.zlp.handler.ExecutorService;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/27 20:47
 * @description：常规下单
 */

public interface NormalOrderCreateExecutorService extends ExecutorService {
}
