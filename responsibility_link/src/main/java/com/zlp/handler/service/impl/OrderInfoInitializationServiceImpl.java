package com.zlp.handler.service.impl;

import com.zlp.aop.InvoicingBiz;
import com.zlp.common.AbstractConditionBaseService;
import com.zlp.dto.CompanyInfoDTO;
import com.zlp.dto.OrderInfoInitializationDTO;
import com.zlp.dto.OrderDTO;
import com.zlp.handler.OrderServiceContextUtils;
import com.zlp.handler.ProcessName;
import com.zlp.handler.ServiceContext;
import com.zlp.handler.ServiceResponse;
import com.zlp.handler.service.OrderInfoInitializationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/28 13:15
 * @description：订单信息初始化（企业信息）
 */
@Service
@Slf4j(topic = "OrderInfoInitializationServiceImpl")
@InvoicingBiz(value = "OrderInfoInitializationServiceImpl", order = 1, processName = ProcessName.Initialization_Order_Data)
public class OrderInfoInitializationServiceImpl extends
        AbstractConditionBaseService implements OrderInfoInitializationService {




    @Override
    public boolean condition(ServiceContext context) {
        if (super.condition(context)) {
            return true;
        }
        return false;
    }

    @Override
    public void process(ServiceContext context) {


        log.info("process.req context={}",context);

        OrderDTO order = OrderServiceContextUtils.getOrder(context);
        /*//初始化OrderDTO信息
        OrderDTO order = OrderServiceContextUtils.getOrder(context);
        WebApiResult<StaffMessageDTO> staffMessageDTOWebApiResult = customerApi.getStaffMessageByUserId(UserThreadLocal.getLocal().getUid());
        if (!staffMessageDTOWebApiResult.getCode().equals(Code.Code_Success.getName()) || staffMessageDTOWebApiResult.getData() == null) {
            throw new ServiceException(Code.Code_Staff_Info_Error, ServiceException.Level.ERROR);
        }
        Long employeeId = staffMessageDTOWebApiResult.getData().getStaffId();

        order.setClerkId(String.valueOf(employeeId));

        if (order.getOrderType().equals(OrderType.Purchase)) {
            order.setIsPayed(PaymentStatus.UnPaid);
        } else if (order.getOrderType().equals(OrderType.Sale)) {
            order.setIsPayed(PaymentStatus.UnReceive);
        }


        OrderInfoInitializationDTO orderInfoInitializationDTO = new OrderInfoInitializationDTO();

        OrderMerchandiseListDTO orderMerchandiseList = OrderServiceContextUtils.getOrderMerchandiseList(context);


        //获取企业信息
        CompanyInfoDTO requestCompanyInfoDTO = new CompanyInfoDTO();
        requestCompanyInfoDTO.setRelationId(String.valueOf(order.getCustomerId()));
        if (orderMerchandiseList.getOrderType().getValue().equals(OrderType.Purchase.getValue())) {
            requestCompanyInfoDTO.setAccountType(DepartmentEnum.supplier);
            //代客下单 传交易户企业ID
            if (OrderCategory.ORDER_CATEGORY_AGENT_ORDER.equals(order.getOrderCategory())) {
                requestCompanyInfoDTO.setTradeCompanyId(order.getTradeCompanyId());
            }
        } else {
            requestCompanyInfoDTO.setAccountType(DepartmentEnum.sale_customer);
        }

        WebApiResult<CustomerInfoDTO> customerInfoDTO = policyBroLoanApi.getCustomerOrSupplierByCondition(requestCompanyInfoDTO);
        CustomerInfoDTO data = customerInfoDTO.getData();
        if (!customerInfoDTO.getCode().equals(Code.Code_Success.getName()) || ArrayUtil.isEmpty(data)) {
            throw new ServiceException(Code.Code_Company_Info_Error, ServiceException.Level.ERROR);
        }
        //来源类别是代理商、订单类别是交易户下单需要查询代理商企业ID--》从企业信息中查询
        ResourceType resourceType = data.getResourceType();
        OrderCategory orderCategory = order.getOrderCategory();
        if (!ObjectUtils.isEmpty(orderCategory) && ResourceType.AGENT.equals(resourceType) && OrderCategory.ORDER_CATEGORY_TRADE_ORDER.equals(orderCategory)) {
            order.setAgentCompanyId(data.getAgentCompanyId());
        }
        order.setCustomerId(data.getCustomerId());
        //销售客户名称
        order.setCustomerName(data.getPurchaseCompanyName());
        order.setPurchaseEnterpriseId(data.getPurchaseCompanyId());
        order.setPurchaseEnterpriseName(data.getPurchaseCompanyName());
        order.setPurchaseEnterpriseShortName(data.getPurchaseShortName());
        order.setSupplierId(data.getSupplierId());
        order.setSupplierEnterpriseId(data.getSaleCompanyId());
        order.setSupplierEnterpriseName(data.getSaleCompanyName());
        order.setSellerTaxpayerNature(data.getSaleTaxPersonType() == null ? null : Long.valueOf(data.getSaleTaxPersonType().getIndex()));

        order.setPurchaseTaxpayerNature(data.getPurchaseTaxPersonType() == null ? null : Long.valueOf(data.getPurchaseTaxPersonType().getIndex()));
        //供应商名称
        order.setSupplierName(data.getSaleCompanyName());
        order.setSupplierEnterpriseShortName(data.getSaleShortName());
        Long enterpriseId = UserThreadLocal.getCid();
        if (OrderCategory.ORDER_CATEGORY_AGENT_ORDER.equals(order.getOrderCategory())) {
            enterpriseId = order.getTradeCompanyId();
        }
        order.setEnterpriseId(enterpriseId);
        order.setCreateUser(UserThreadLocal.getUname());
        order.setCreateUserId(UserThreadLocal.getUid());
        order.setSystemId(UserThreadLocal.getSysId());
        order.setSupplyDemandConnection(data.getSaleConnectionFlag());

        if (order.getCreateDate() == null) {
            order.setCreateDate(new Date());
        }*/
        OrderInfoInitializationDTO orderInfoInitializationDTO = new OrderInfoInitializationDTO();
        CompanyInfoDTO companyInfoDTO = new CompanyInfoDTO();
        companyInfoDTO.setAgentCompanyId(1L);
        orderInfoInitializationDTO.setCompanyInfoDTO(companyInfoDTO);
        //塞进Body中
        context.createResponseBody(orderInfoInitializationDTO);

        ServiceResponse response = context.getResponse();
    }
}
