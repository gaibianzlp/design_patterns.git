package com.zlp.handler.service;

import com.zlp.service.BaseService;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/28 13:13
 * @description：订单新增信息初始化（企业信息）
 */
public interface OrderInfoInitializationService extends BaseService {

}
