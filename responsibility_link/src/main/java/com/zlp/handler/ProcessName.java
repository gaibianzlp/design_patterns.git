package com.zlp.handler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/4/9 14:18
 * @description：订单执行枚举配置
 */
public enum ProcessName {

    //下单主流程逻辑
    Null_Default("def", "默认值"),
    Order_Approval_Create_User_Verify("CreateApprovalUserVerifyServiceImpl", "审批数据主账号禁止操作验证"),
    Initialization_Merchandise_Data_FixedAssets_Car_Order("FixedAssetsCarOrderInitMerchandiseInfoServiceImpl", "固定资产车辆下单--外购商品,自有产品,商品归类初始化"),
    Initialization_Company_Data_FixedAssets_Car_Order("FixedAssetsCarOrderInitCompanyDataServiceImpl", "固定资产车辆下单--公司信息初始化"),
    Initialization_Order_Data("OrderInfoInitializationServiceImpl", "初始化订单基本信息"),
    INITIALIZATION_ORDER_CATEGORY("OrderCategoryInitializationServiceImpl", "初始化订单类别"),
    Initialization_Company_Data("CompanyInfoInitializationServiceImpl", "初始化公司信息（根据当前企业Id+传过来的企业Id获取两者的关系Id）"),
    INITIALIZATION_WAREHOUSE_DATA("OrderMerchandiseWarehouseInitializationServiceImpl", "初始化异地仓信息"),
    INITIALIZATION_WAREHOUSE_CHILD_DATA("OrderMerchandiseWarehouseChildInitializationServiceImpl", "初始化异地仓子商品(搭售、买赠)信息"),
    Initialization_PayKind_Data("PayKindInitializationServiceImpl", "付款方式初始化"),
    Initialization_Order_Status("OrderStatusInitializationServiceImpl", "初始化订单状态信息"),
    Initialization_Merchandise_Auth_Pull_Data("MerchandiseInfoAuthPullServiceImpl", "获取授权商品信息"),
    Initialization_Merchandise_Non_Auth_Pull_Data("MerchandiseInfoNonAuthPullServiceImpl", "获取无连接商品信息"),
    Initialization_Sync_Order_Data("OrderSyncConnectInitializationServiceImpl", "订单同步初始化目标企业 创单人信息"),
    Initialization_Merchandise_Data("MerchandiseInfoInitializationServiceImpl", "初始化商品信息"),
    Initialization_FixedAssets_Merchandise_Data("FixedAssetsMerchandiseInfoInitializationServiceImpl", "初始化固定资产商品信息"),
    Initialization_Merchandise_Product_Data("OrderProductInfoInitializationServiceImpl", "初始化商品产品信息"),
    Initialization_Merchandise_DealPrice("MerchandiseDealPriceInitializationServiceImpl", "商品成交价初始化"),

    Verify_Order_Merchandise_FacePrice("MerchandiseFacePriceVerificationServiceImpl", "商品面价验证"),
    Verify_Order_PayKind("PayKindVerificationServiceImpl", "付款方式有效性验证"),
    Verify_Order_Merchandise_Own_Subject_Info("MerchandiseOwnAndSubjectVerificationServiceImpl", "自有商品信息及科目验证"),
    Verify_Order_Merchandise_Info("MerchandiseVerificationServiceImpl", "商品基本信息验证"),


    Create_Order_NO("billNOCreateImpl", "创建订单单据编号 用该编号作为订单号"),
    Verify_Order_BasePolicy("BasePolicyVerifyServiceImpl", "基础授权验证"),
    Verify_Order_Policy_Verify("PolicyVerificationService", "商品政策验证，并且将政策信息放入请求实体"),
    Query_Order_Info("QueryOrderInfoByConditionServiceImpl", "根据条件获取订单信息"),
    Update_Order_Pay_Kind_Info("UpdateOrderPayKindInfoServiceImpl", "修改订单付款方式"),

    Calculation_Order_BasePrice("BasePriceCalculationServiceImpl", "基本价格计算不包含任何政策"),
    Calculation_Order_Policy("PolicyCalculationServiceImpl", "政策计算"),

    Locker_Order_Open("OpenLockerServiceImpl", "订单加锁"),
    Order_Original_Cut_Bill_Get("OrderCutBillGetServiceImpl", "订单原始割账金额查询"),
    ORDER_ORIGINAL_CREDIT_BILL_GET("OrderCutBillGetServiceImpl", "订单原始授信记录查询"),
    WAREHOUSE_CALCULATION("OrderMerchandiseWarehouseCalculationServiceImpl", "异地仓金额计算"),
    Calculation_Order_Detail_Total_Calculation("OrderDetailTotalCalculationServiceImpl", "订单明细金额汇总 并且把商品总价转为2 位"),
    Calculation_Order_CutBill("CutBillCalculationServiceImpl", "割账计算"),
    Non_Money_Calculation_Order_Policy("NonMoneyPolicyCalculationServiceImpl", "订单政策计算-非金额计算"),
    Calculation_Order_Tax("TaxCalculationServiceImpl", "税额计算"),

    Calculation_Order_Total_Calculation("OrderTotalCalculationServiceImpl", "订单总金额汇总，并且把商品总价转为2位"),


    Verify_Order_Payment_Info("PaymentTermsServiceVerificationImpl", "订单付款方式验证"),
    Verify_Order_Merchandise_DealPrice("MerchandiseDealPriceVerificationServiceImpl", "商品成交价验证"),

    Amount_Purchase_Verify("OrderAvailablePurchaseMoneyVerificationServiceImpl", "账款-采购方验证"),
    Amount_Batch_Purchase_Verify("OrderBatchAvailablePurchaseMoneyVerificationServiceImpl", "账款-采购方批量验证"),
    Amount_Sale_Verify("OrderAvailableSaleMoneyVerificationServiceImpl", "账款-销售方验证"),
    Amount_Occupy_Calculation("Amount_Occupy_Calculation", "账款-信用额度占用计算"),
    Purchase_Amount_Occupy_Calculation("PurchaseAmountOccupyServiceImpl", "采购无链接-信用额度占用计算"),

    Verify_Total_Tax_And_Deal_Total_Price("TotalTaxAndDealTotalPriceVerificationServiceImpl", "验证增值税总税额,成交总价是否与后台一致"),

    Insert_OrderInfo_Into_DB("OrderCreateServiceImpl", "订单主表数据落库"),
    Update_OrderInfo_Into_DB("OrderUpdateServiceImpl", "更新订单数据到数据库"),
    Initialization_Insert_Order_Merchandise_Info_DB("OrderMerchandiseCreateInitializationServiceImpl", "订单商品数据基本信息落库前初始化"),

    Stock_Occupy_Calculation("StockOccupyServiceImpl", "库存占用"),
    Insert_Order_Merchandise_Info_DB("OrderMerchandiseCreateServiceImpl", "订单商品数据落库"),

    Amount_Occupy_Create("AmountOccupyServiceImpl", "账款-信用额度占用落库"),
    Approval_Data_Delete("RemoveApprovalDataServiceImpl", "移除审批中心数据", true),
    Order_Remove_History("OrderHistoryReleaseServiceImpl", "订单更新 移除割账,政策历史记录"),
    CutBill_Occupy_Calculation("AmountOccupyServiceImpl", "割帐落库"),
    Order_Remove_CutBill_History("CutBillReleaseServiceImpl", "订单更新 移除割账记录"),
    Locker_Order_Close("CloseLockerServiceImpl", "订单解锁", true),

    //预订单落库相关
    PRE_ORDER_INITIALIZATION("PreOrderDetailInitializationServiceImpl", "预订单信息初始化"),
    PRE_ORDER_DETAIL_ADD_POLICY("PreOrderDetailAddPolicyServiceImpl", "预订单详情添加已匹配的政策信息"),
    PRE_ORDER_DETAIL_CREATE("PreOrderDetailCreateServiceImpl", "预订单详情入库"),
    PRE_ORDER_CREATE("PreOrderCreateServiceImpl", "预订单入库"),
    CALCULATION_PRE_ORDER_POLICY("PolicyComputationServiceImpl", "预订单政策计算"),
    PRE_ORDER_SUBMIT("PreOrderSubmitServiceImpl", "预订单政策计算并提交订单"),
    INFORMATION_VERIFICATION("PreOrderVerificationServiceImpl", "预订单提交信息验证"),
    //预订单发送消息
    PRE_ORDER_SEND_MESSAGE("PreOrderSendMessageServiceImpl", "预订单发送消息"),
    SYNCHRONIZATION_DATA("SynchronizationDataServiceImpl", "初始化预订单同步信息"),
    PRE_ORDER_STATUS_CHANGE("PreOrderSubmitStatusChangeServiceImpl", "预订单提交后，订单状态更改"),

    Order_Payment_Send_MQ_Installment_Msg("InstallmentPaymentServiceImpl", "分期付款 付款支付单记录落库，推送预收或者预付金额", true),
    Order_Payment_Send_MQ_Fix_Car_Msg("FixedAssetsCarPaymentServiceImpl", "固定资产车辆，推送预收或者预付金额", true),
    Order_Payment_Send_MQ_Approve_Installment_Msg("ApprovalPassInstallmentServiceImpl", "分期付款 付款支付单记录落库，推送预收或者预付金额", true),
    Order_Payment_Send_MQ_Sync_Installment_Msg("SyncOrderInstallmentServiceImpl", "分期付款 付款支付单记录落库，推送预收或者预付金额", true),


    Order_Sync_Send_MQ_Msg("OrderSyncMessageServiceImpl", "向消息中心发送同步订单mq-msg", true),
    Order_Fix_Sale_Sync_Send_MQ_Msg("FixSaleOrderSyncMessageServiceImpl", "向消息中心发送固定资产销售同步订单mq-msg", true),


    Approval_Data_Send_MQ_Msg("SendPendingMsgServiceImpl", "向消息中心发送审批数据mq-msg", true),
    //总是要发消息
    Approval_Data_Send_MQ_Msg_Auto_Price("AutoPriceSendPendingMsgServiceImpl", "自主定价订单--向消息中心发送审批数据mq-msg", true),

    Order_Accept_Change_To_Manufacture_Apply("OrderChangeToManufactureServiceImpl", "将订单信息转换 申请生产信息", true),
    Order_Accept_Manufacture_Apply("AcceptOrderManufactureOrderServiceImpl", "受订订单向生产发送收订派单信息mq-msg", true),

    //订单接收到消息后 进行同步操作
    Order_Sync_Recipient("OrderSyncServiceImpl", "接收订单同步消息，并开始同步业务"),
    Order_SyncOrderContext_Change_To_NormalContext("ChangeSyncOrderToPendingServiceImpl", "将同步信息上下文关联类转换为普通上下文关联"),


    //订单接收到审批消息后进行 审批后处理
    Order_Approval_Initialization("ApprovalOrderInitializationImpl", "接收审批消息，查询订单数据信息"),
    Order_Approval_Agree("ApprovalAgreeServiceImpl", "接收审批消息，审批通过"),
    Order_Approval_Change_Context_To_Normal("ChangeApprovalOrderToNormalContextServiceImpl", "将审批中心数据转成正常下单上下文关联类型"),

    Order_Merchandise_Info_Query_Not_Change_Order("OrderInfoGetNotChangeOrderBodyServiceImpl", "查询出所有订单商品 为无链接 采购 有款下单准备"),
    Order_Cash_Pay_Change_Order("CashPayOrderPaymentChangeServiceImpl", "有款下单订单状态信息设置，如果全部审核则标识为完成，如果部分审核则标识部分完成"),
    Fixed_Assets_Car_Order_Warehousing_Plan("FixedAssetsCarOrderWarehousingPlanServiceImpl", "固定资产车辆下单生成入库计划单"),
    Order_Change_Payment_Status_Update_DB("ChangeOrderPaymentStatusServiceImpl", "订单支付状态更新落库"),
    //发送barcode 信息
    BarCodeNonConnectSendMsg("BarCodeSendMsgServiceImpl", "受订商品发送barcode mq-msg", true),
    //接受到支付完成后的逻辑
    Order_Payment_Completed_Data_Initialization("OderInfoQueryServiceImpl", "接受到审批收费款完成后 基本信息初始化"),
    Order_Payment_Completed_Update_Invoicing_Pay_Status("OrderPaymentCompleteServiceImpl", "接受到审批收费款完成后 更新支付状态（订单 和 付款明细）"),
    Fixed_Assets_Car_Collection_Initialize_Service("FixedAssetsCarCollectionInitializeServiceImpl", "固定资产车辆采购收款消息初始化"),
    Fixed_Assets_Car_Check_Ticket_Msg_Service("FixedAssetsCarCheckTicketMsgServiceImpl", "固定资产车辆采购收付款验票消息发送mq-msg", true),
    Fixed_Assets_Car_Taxes_Msg_Service("FixedAssetsCarTaxesMsgServiceImpl", "固定资产车辆采购收付款税费消息发送mq-msg", true),

    //申请生产
    Apply_Manufacture_Order_Initialization("ApplyManufactureProductInitializationServiceImpl", "申请生产订单信息初始化"),
    Apply_Manufacture_Order_Initialization_International("ApplyManufactureInternationalSendMqMsgServiceImpl","生产采购分配数量发送消息给国际业务"),
    Locker_Order_Status_Change_Open("OrderStatusChangeOpenLockerServiceImpl", "订单状态改变加锁"),
    Apply_Manufacture_Product_Initialization("ApplyManufactureProductInitializationServiceImpl", "申请生产商品信息初始化"),
    Apply_Manufacture_Change_To_InStock_Status("ManufactureStatusChangeToInStockServiceImpl", "申请生产技术产品占库存"),
    Apply_Manufacture_Change_Status("ManufactureStatusChangeToPendingServiceImpl", "申请生产订单受订状态转为待制状态"),
    Locker_Order_Status_Change_Close("OrderStatusChangeCloseLockerServiceImpl", "订单状态改变解锁", true),

    Cancel_Order_Accept_Manufacture_Apply("AcceptOrderCancelManufactureOrderServiceImpl", "取消技术受订订单向生产发送收订派单信息mq-msg", true),
    Apply_Manufacture_Send_Apply_Making_Mq_Msg("ApplyManufactureSendMqMsgServiceImpl", "申请生产发送-mq消息", true),


    //订单状态change

    Order_Change_Order_Initialization("OrderInfoInitializationServiceImpl", "订单扭转订单信息初始化"),
    Order_Change_Merchandise_Initialization("OrderDetailChangeInitializationServiceImpl", "订单扭转商品信息初始化"),
    Order_Change_Order_To_Other("OrderChangeToOtherStatusServiceImpl", "将订单扭转到其他状态"),

    ///订单审批前验证
    Order_Merchandise_Info_Query("OrderInfoGetServiceImpl", "订单信息 和 全部下单商品 信息拉取"),

    Order_Update_Info_Query("OrderUpdateOrderInfoGetServiceImpl", "订单更新原始订单信息查询，不需要全部订单商品信息"),
    Amount_Purchase_Non_Connection_Verify("PurchaseNonConnectionMoneyVerificationServiceImpl", "采购无链接订单 有款下单账款判断"),
    AMOUNT_PURCHASE_TRADE_VERIFY("AgentAvailableBalanceVerificationServiceImpl", "交易户、代理商下单，有款下单验证--采购方"),
    AMOUNT_SALE_TRADE_VERIFY("AgentSaleAvailableBalanceVerificationServiceImpl", "交易户、代理商下单，有款下单验证--销售方"),

    //非合约下单发送支付消息
    Order_Payment_Send_MQ_NonContractOrder_Msg("NonContractOrderPrepaymentServiceImpl", "非合约下单 发送预付消息"),

    //出库结案
    Order_Return_In_Stock_Service_Impl("OrderReturnInStockServiceImpl", "出库结案回在库"),
    Order_Out_Bound_Service_Impl("OrderOutBoundServiceImpl", "出库结案指定废品回受订"),
    Order_Out_To_Warehouse_Service_Impl("OrderOutToWarehouseServiceImpl", "出库结案指定废品回在库"),
    Order_Out_Stock_Service_Impl("OrderOutStockServiceImpl", "出库结案转在途"),
    Order_Out_Consignment_On_The_Way_Service_Impl("OrderOutConsignmentOnTheWayServiceImpl", "出库结案托运单在途扭转"),
    Order_Out_Consignment_Data_Cleaning_Service_Impl("OrderOutConsignmentDataCleaningServiceImpl", "出库结案托运单数据清洗"),
    Order_Consignment_Note_Deleted_Service_Impl("OrderConsignmentNoteDeletedServiceImpl", "出库结案托运单删除"),
    Order_Out_Data_Recording_Service_Impl("OrderOutDataRecordingServiceImpl", "订单出库结案数据记录"),
    Cash_On_Delivery_Payment_Service_Impl("CashOnDeliveryPaymentServiceImpl", "货到付款-推收款消息", true),

    //订单修改
    OrderStatusVerify("OrderStatusVerificationServiceImpl", "订单修改前验证是否都在待审状态"),
    Order_Policy_Info_Query("OrderPolicyInfoGetServiceImpl", "订单政策信息查询"),

    Approval_Order_Merchandise_Policy_Service_Impl("ApprovalOrderMerchandisePolicyServiceImpl", "审批同步订单商品政策"),

    Order_Excess_Service_Impl("OrderExcessServiceImpl", "订单超额标识处理"),
    Order_Excess_Message_Service_Impl("OrderExcessMessageServiceImpl", "订单超额消息通知", true),

    Order_Bulk_InitializationServiceImpl("OrderBulkInitializationServiceImpl", "订单信息批量初始化"),
    Order_Profit_Message_Service_Impl("OrderProfitMessageServiceImpl", "销售代客下单利润消息推送", true),

    Order_Application_Production_Initialization_Service_Impl("OrderApplicationProductionInitializationServiceImpl", "订单申请生产初始化"),
    Order_Apply_For_Production_Send_Mq_Service_Impl("OrderApplyForProductionSendMqServiceImpl", "下单申请生产消息推送"),
    Order_Production_Purchase_Distribution_Service_Impl("OrderProductionPurchaseDistributionServiceImpl", "订单生产采购分配"),
    Order_Urgent_Need_For_Procurement_Service_Impl("OrderUrgentNeedForProcurementServiceImpl", "订单急需采购"),

    //超链订单
    Super_Chain_Object_Judgment_Service_Impl("SuperChainObjectJudgmentServiceImpl", "超链对象判断"),
    Superchain_Merchandise_Init_Service("SuperchainMerchandiseInitServiceImpl", "超链商品初始化"),
    Superchain_Merchandise_Status_Change_Service("SuperchainMerchandiseStatusChangeServiceImpl", "超链商品状态变更"),

    //托外受托单
    Process_Order_Create_Service("ProcessOrderCreateServiceImpl", "托外受托单落库"),
    Process_Order_Merchandise_Modify_Service("ProcessOrderMerchandiseModifyServiceImpl", "托外受托单修改"),

    //固定资产设备采购
    Device_Purchase_Send_Pending_Msg_Service("DevicePurchaseSendPendingMsgServiceImpl", "固定资产设备采购发送审批消息", true),
    //固定资产叉车采购
    Forklift_Purchase_Send_Pending_Msg_Service("ForkliftPurchaseSendPendingMsgServiceImpl", "固定资产叉车采购发送审批消息", true),
    ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;

    public Boolean getMqFlag() {
        return mqFlag;
    }

    public void setMqFlag(Boolean mqFlag) {
        this.mqFlag = mqFlag;
    }

    private Boolean mqFlag;


    ProcessName(String name, String value) {
        this.name = name;
        this.value = value;
    }

    ProcessName(String name, String value, Boolean mqFlag) {
        this.name = name;
        this.value = value;
        this.mqFlag = mqFlag;
    }

    public static List<ProcessName> getAllEnumType(String val) {
        List<ProcessName> resultList = new ArrayList<>();
        for (ProcessName type : ProcessName.values()) {
            if (!type.equals(ProcessName.Null_Default)) {
                resultList.add(type);
            }

        }
        return resultList;
    }


    @Override
    public String toString() {
        return super.toString();
    }
}