package com.zlp.handler;
/**
 *
 */
public interface ExecutorService {

    /**
     * 服务开始
     *
     */
    void start(ServiceContext serviceContext);

    /**
     * 执行所有的业务
     *
     * @param serviceContext
     */
    void run(ServiceContext serviceContext);

    /**
     * @param serviceContext
     *
     * 执行业务
     */
    void execute(ServiceContext serviceContext);

    /**
     * @param serviceContext

     * 服务结束(返回参数)
     */
    void stop(ServiceContext serviceContext);
}
