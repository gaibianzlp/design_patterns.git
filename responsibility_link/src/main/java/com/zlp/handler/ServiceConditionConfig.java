package com.zlp.handler;

import lombok.Data;

import java.util.List;

/**
 * @author ：yangxingwen
 * @date ：Created in 2019/3/26 10:29
 * @description：各个业务执行条件
 */
@Data
public class ServiceConditionConfig {
    /**
     * 所有加入当前集合的类都会执行 反之不执行 如果
     */
    private List<String> conditionList;


    /**
     * 所有加入当前集合的类都会执行 反之不执行 如果
     */
    private List<ProcessName> processNameList;

    /**
     * 是否执行全部
     */
    private Boolean runAll;
}
