package com.zlp.handler;

import lombok.Data;

@Data
public class ChangeStatusConfig {
    /**
     *
     * 扭转层级 顶级为0 数值越大 层次越低
     */
    private int changeLevel;
    private OrderStatus currentStatus;
    /**
     * 下一个层次
     */
    private ChangeStatusConfig nextStatus;

    public void addNext(ChangeStatusConfig nextStatus)
    {
        this.nextStatus=nextStatus;
        this.nextStatus.setChangeLevel(this.changeLevel+1);
    }
}