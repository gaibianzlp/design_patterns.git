package com.zlp.dto;

import lombok.Data;



/**
 * @author ：wangyuling
 * @date ：Created in 2019/4/2 9:56
 * @description:订单初始化DTO
 */
@Data
public class OrderInfoInitializationDTO {
    /**
     * 企业信息
     */
    private CompanyInfoDTO companyInfoDTO;

}
