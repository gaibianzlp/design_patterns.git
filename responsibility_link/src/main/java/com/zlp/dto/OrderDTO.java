package com.zlp.dto;


import lombok.Data;

@Data
public class OrderDTO {

    /**
     * 订单Id
     */
    private Long orderId;
    /**
     * 订单编号
     */
    private String orderNo;
    private Long amt;
    private String address;
    private Integer orderType;
}
