package com.zlp.dto;

import lombok.Data;


/**
 * 获取客户授权的公司信息实体
 *
 * @author xuhaojun
 * @date 2019/3/20
 */
@Data
public class CompanyInfoDTO {

    /**
     * 公司企业Id
     */
    private Long companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司简称
     */
    private String shortname;



    /**
     * 员工ID
     */
    private Long employeeId;

    /**
     * 关系Id （供应商或者销售客户,可能根据多个relationId查询。所以改为String，例如:"1,2,3,4"）
     */
    private String relationId;

    /**
     * 客户企业ID (因为有可能是多个就用字符串格式 例如："1,2,3,4")
     */
    private String customerId;

    /**
     * 供应商ID
     */
    private Long supplierId;

    /**
     * 分页index
     */
    private Integer pageIndex;

    /**
     * 分页大小
     */
    private Integer pageSize;

    /**
     * 代理商企业ID
     */
    private Long agentCompanyId;

    /**
     * 交易户企业ID
     */
    private Long tradeCompanyId;

    /**
     * 订单号
     */
    private String orderNo;
}
