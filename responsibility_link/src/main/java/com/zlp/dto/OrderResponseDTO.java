package com.zlp.dto;

import lombok.Data;

import java.util.List;

/**
 * @author ：wangyuling
 * @date ：Created in 2019/5/7 13:55
 * @description：订单返回DTO
 */
@Data
public class OrderResponseDTO {
    /**
     * 订单Id
     */
    private Long orderId;
    /**
     * 订单编号
     */
    private String orderNo;

}