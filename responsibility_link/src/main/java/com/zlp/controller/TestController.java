package com.zlp.controller;

import com.zlp.dto.OrderDTO;
import com.zlp.handler.ServiceContext;
import com.zlp.handler.service.NormalOrderCreateExecutorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final NormalOrderCreateExecutorService normalOrderCreateExecutorService;


    /**
     * 采购提交订单
     *
     * @param orderDTO
     * @return
     */
    @PostMapping(value = "/purchase/createOrder")
    public Object createOrder(@RequestBody OrderDTO orderDTO) {
        ServiceContext context = new ServiceContext();
        orderDTO.setOrderType(1);
        context.createRequestBody(orderDTO);
        //基本执行条件
        normalOrderCreateExecutorService.execute(context);
        return context.getResponse();
    }




}
